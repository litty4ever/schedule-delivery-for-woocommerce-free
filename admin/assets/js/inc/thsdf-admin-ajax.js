/*
 * Product delivery admin ajax function.
 */
jQuery(function($) {
  window.onload = function() {
    var language = window.navigator.userLanguage || window.navigator.language;
  }
jQuery(document).ready(function($){
  // console.log('sddsd');
  jQuery('.plan_delivery_cancelled_items_view').hide();
  jQuery('.button.date_specific_cancel').hide();

   document.addEventListener("DOMContentLoaded", function(event) {
        var $load = document.getElementById("load");
         
        var removeLoading = setTimeout(function() {
            $load.className += " loader-removed";
        }, 500);
    });
   jQuery("input.have_ptdt_calendar").each(function(){
    var ptdt_calendar_item_id = $(this).val();
    if(ptdt_calendar_item_id != ''){
      $('input[name="refund_order_item_qty['+ptdt_calendar_item_id+']"]').prop('disabled', true);
      $('input[name="refund_line_total['+ptdt_calendar_item_id+']"]').prop('disabled', true);
    }
  });
  save_calender_general_settings();
  default_general_settings();
  save_calender_display_settings();
  default_display_settings();
  order_edit_checkbox();
  order_item_cancelled_date_specific();
  order_item_completed_date_specific();
  var product_id = '';
  var product_date = '';
  order_item_refund_date_specific(product_id,product_date);
  order_item_refund_cancel_date_specific(product_id,product_date);
  ordered_item_delete_from_cancelled_table();
  
  
}); 

function save_calender_general_settings(){
  var settings_info = {};
  var settings_data = [];
  var plan_display_data = [];
  var plan_display = [];

  var requestSent = false;
  jQuery( document ).on( 'click', '.save_calender_settings', function(e) {
    var holidays_data = [];
    e.preventDefault();
    
    jQuery('.thsdf_updated_mssg').html('');

    jQuery(".thpt-form-table .spinner").show();
    var product_id = jQuery("input[name=g_product_id]").val();
    var week_start = jQuery('select[name="g_week_start"]').val();
    var delivery_day = [];
    var delivery_day_f = [];
    jQuery("input[name='g_delivery_day[]']:checked").each(function() {
         delivery_day_f.push(this.value);
    });
    var delivery_day = delivery_day_f.filter( onlyUnique );
    
    var start_date = jQuery("input[name=g_start_date]").val();


    var end_date = jQuery("input[name=g_end_date]").val();
    jQuery(".thpt-general-settings-row .g-holidays").each(function() {
        holidays_data.push(this.value);
        if((this.value) == ''){
            $(this).closest("tr").not(':nth-child(1)').remove();
        }
    });
    var holidays = holidays_data.filter( onlyUnique );

    jQuery("input[name='g_plan_display[]']:checked").each(function() {
        plan_display_data.push(this.value);
    });
    var plan_display = plan_display_data.filter( onlyUnique );

    if( product_id != '' ) {
      settings_info['product_id'] = product_id;
      settings_info['week_start'] = week_start;
      settings_info['delivery_day'] = delivery_day;
      settings_info['start_date'] = start_date;
      settings_info['end_date'] = end_date;
      settings_info['holidays'] = holidays;
      settings_info['plan_display'] = plan_display;
    } 

    function onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }
    
    if((delivery_day.length != 0) && (end_date != '')){
      // console.log(settings_info);
      var general_nonce = admin_ajax_script.general_settings_nonce;
      jQuery('.ptdt-setting-loader').show();
      jQuery.ajax({
        url : ajaxurl,
        type : 'post',
        data : {
          action : 'save_calender_settings',
          product_id : product_id,
          settings_data : settings_info,
          nonce : general_nonce
        },
        success : function( response ) {

          jQuery('.thsdf_updated_mssg').html(response);
          jQuery('.ptdt-setting-loader').hide();
          // return false;
        }

      });
      // return false;
    }
    // return false;
  });
}

function default_general_settings(){
  jQuery( document ).on( 'click', '.default-single-general-settings', function(e) {
    e.preventDefault();
    if(confirm("Are you sure you want to reset to default settings? all your changes will be deleted.")){
      var d = new Date();
      var month = d.getMonth()+1;
      var day = d.getDate();
      var current_date = d.getFullYear() + '-' +
      (month<10 ? '0' : '') + month + '-' +
      (day<10 ? '0' : '') + day;

      var week_start = 'sunday';
      var settings_info = {};
      var settings_data = [];
      var delivery_day = [];
      var start_date = '';
      
      var end_date = '';
      var holidays = [];
      
      var plan_display = [];

      var product_id = jQuery("input[name=g_product_id]").val();
      jQuery('select[name="g_week_start"]').val('sunday');
      jQuery("input[name='g_delivery_day[]']:checked").each(function() {
        jQuery(this).attr('checked',false);
      });
      jQuery("input[name=g_start_date]").val('');

      jQuery("input[name=g_end_date]").val('');
      jQuery(".thpt-general-settings-row .g-holidays").val('');

      jQuery("input[name='g_plan_display[]']:checked").each(function() {
          jQuery(this).attr('checked',false);
      });   

      $(".holidays-list.thptadmin-dynamic-row-table").find("tr:gt(0)").remove();

      if( product_id != '' ) {
        settings_info['product_id'] = product_id;
        settings_info['week_start'] = week_start;
        settings_info['delivery_day'] = delivery_day;
        settings_info['start_date'] = start_date;
        
        settings_info['end_date'] = end_date;
        settings_info['holidays'] = holidays;
        
        settings_info['plan_display'] = plan_display;
      }
      var general_nonce = admin_ajax_script.general_settings_nonce;
      jQuery('.ptdt-setting-loader').show();
      jQuery.ajax({
        url : ajaxurl,
        type : 'post',
        data : {
          action : 'default_general_settings',
          product_id : product_id,
          settings_data : settings_info,
          nonce : general_nonce
        },
        success : function( response ) {
          jQuery('.thsdf_updated_mssg').html(response);
          jQuery('.ptdt-setting-loader').hide();
        }
      });
    } else {
      return false;
    }
  });
}

function save_calender_display_settings(){
  var settings_info = {};
  var settings_data = [];

  jQuery( document ).on( 'click', '.ptdt-single-display-settings-save', function(e) {
    e.preventDefault();
    jQuery('.thsdf_display_updated_mssg').html('');
    jQuery(".thpt-form-table .spinner").show();
    var product_id_display = jQuery("input[name=g_product_id]").val();
    var primary_color = jQuery('input[name="g_primary_color"]').val();  
    var secondary_color = jQuery('input[name="g_secondary_color"]').val();   
    var holiday_columns = jQuery('input[name="g_holiday_columns"]').val();    
    var tooltip_bg = jQuery('input[name="g_tooltip_bg"]').val();    
    var general_color = jQuery('input[name="g_general_color"]').val();   
    var input_value = jQuery('input[name="g_input_value"]').val();    
    var tooltip = jQuery('input[name="g_tooltip"]').val();    
    
    if( product_id_display != '' ) {
      settings_info['product_id'] = product_id_display;
      settings_info['primary_color'] = primary_color;
      settings_info['secondary_color'] = secondary_color;
      settings_info['holiday_columns'] = holiday_columns;
      settings_info['tooltip_bg'] = tooltip_bg;
      settings_info['general_color'] = general_color;
      settings_info['input_value'] = input_value;
      settings_info['tooltip'] = tooltip;
    }
    jQuery('.ptdt-setting-loader').show();
    var display_nonce = admin_ajax_script.display_settings_nonce;
    jQuery.ajax({
      url : ajaxurl,
      type : 'post',
      data : {
        action : 'save_caldr_display_setting',
        product_id : product_id_display,
        settings_data : settings_info,
        nonce : display_nonce
      },

      success : function( response ) {
        jQuery('.ptdt-setting-loader').hide();
        jQuery('.thsdf_display_updated_mssg').html(response);
      }
    });

  });
}

function default_display_settings(){
  var settings_info = {};
  var settings_data = [];
  var primary_field = jQuery('input[name="g_primary_color"]');
  var secondary_field = jQuery('input[name="g_secondary_color"]');
  var holiday_coloumn = jQuery('input[name="g_holiday_columns"]');
  var tooltip_bg_field = jQuery('input[name="g_tooltip_bg"]');
  var general_color_field = jQuery('input[name="g_general_color"]');
  var input_val_field = jQuery('input[name="g_input_value"]');
  var tooltip_val_field = jQuery('input[name="g_tooltip"]');
  jQuery( document ).on( 'click', '.default-single-display-settings', function(e) {
    e.preventDefault();
    if(confirm("Are you sure you want to reset to default settings? all your changes will be deleted.")){
      var product_id_display = jQuery("input[name=g_product_id]").val();
      var primary_color = '#e5e5e6';  
      var secondary_color = '#c4c4c4';   
      var holiday_columns = '#f8f8f8';    
      var tooltip_bg = '#ffffff';    
      var general_color = '#6d6d6d';   
      var input_value = '#43454b';    
      var tooltip = '#444444';
      primary_field.val(primary_color);
      primary_field.parent().find( '.ptdt-display-admin-colorpickpreview' ).css({ backgroundColor: primary_color });
      secondary_field.val(secondary_color);
      secondary_field.parent().find( '.ptdt-display-admin-colorpickpreview' ).css({ backgroundColor: secondary_color });
      holiday_coloumn.val(holiday_columns);
      holiday_coloumn.parent().find( '.ptdt-display-admin-colorpickpreview' ).css({ backgroundColor: holiday_columns });
      tooltip_bg_field.val(tooltip_bg);
      tooltip_bg_field.parent().find( '.ptdt-display-admin-colorpickpreview' ).css({ backgroundColor: tooltip_bg });
      general_color_field.val(general_color);
      general_color_field.parent().find( '.ptdt-display-admin-colorpickpreview' ).css({ backgroundColor: general_color });
      input_val_field.val(input_value);
      input_val_field.parent().find( '.ptdt-display-admin-colorpickpreview' ).css({ backgroundColor: input_value });
      tooltip_val_field.val(tooltip); 
      tooltip_val_field.parent().find( '.ptdt-display-admin-colorpickpreview' ).css({ backgroundColor: tooltip });
      
      if( product_id_display != '' ) {
        settings_info['product_id'] = product_id_display;
        settings_info['primary_color'] = primary_color;
        settings_info['secondary_color'] = secondary_color;
        settings_info['holiday_columns'] = holiday_columns;
        settings_info['tooltip_bg'] = tooltip_bg;
        settings_info['general_color'] = general_color;
        settings_info['input_value'] = input_value;
        settings_info['tooltip'] = tooltip;
      }
      jQuery('.ptdt-setting-loader').show();
      var display_nonce = admin_ajax_script.display_settings_nonce;
      jQuery.ajax({
        url : ajaxurl,
        type : 'post',
        data : {
          action : 'default_display_settings',
          product_id : product_id_display,
          settings_data : settings_info,
          nonce : display_nonce
        },

        success : function( response ) {
          jQuery('.ptdt-setting-loader').hide();
          jQuery('.thsdf_display_updated_mssg').html(response);
        }
      });
    }
  });
}

jQuery( ".ptdt-display-admin-colorpick" ).change(function() {
  var primary_color = jQuery('input[name="g_primary_color"]').val();
  var primary_color_Ok  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(primary_color);
  var secondary_color = jQuery('input[name="g_secondary_color"]').val();
  var secondary_color_Ok  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(secondary_color);
  var holiday_columns = jQuery('input[name="g_holiday_columns"]').val();
  var holiday_columns_Ok  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(holiday_columns);
  var tooltip_bg = jQuery('input[name="g_tooltip_bg"]').val();
  var tooltip_bg_Ok  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(tooltip_bg);
  var general_color = jQuery('input[name="g_general_color"]').val();
  var general_color_Ok  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(general_color);
  var input_value = jQuery('input[name="g_input_value"]').val();
  var input_value_Ok  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(input_value);
  var tooltip = jQuery('input[name="g_tooltip"]').val();
  var tooltip_Ok  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(tooltip);

  if(primary_color_Ok == false){
    jQuery('input[name="g_primary_color"]').val('#e5e5e6');
  }
  if(secondary_color_Ok == false){
    jQuery('input[name="g_secondary_color"]').val('#c4c4c4');
  }
  if(holiday_columns_Ok == false){
    jQuery('input[name="g_holiday_columns"]').val('#f8f8f8');
  }
  if(tooltip_bg_Ok == false){
    jQuery('input[name="g_tooltip_bg"]').val('#ffffff');
  }
  if(general_color_Ok == false){
    jQuery('input[name="g_general_color"]').val('#6d6d6d');
  }
  if(input_value_Ok == false){
    jQuery('input[name="g_input_value"]').val('#43454b');
  }
  if(tooltip_Ok == false){
    jQuery('input[name="g_tooltip"]').val('#444444');
  }
});

// Order Cancelled ajax with specific date.
function order_item_cancelled_date_specific(){
  // var item_total = 0;
  var total_refund_price = 0;
  //var item_total_arr = [];
  $(document).on( 'click', '.ptdt_delivery_cancelled', function(event){
    event.preventDefault();
    
      
      var item_date = $(this).data('itemdate');
      var item_id = $(this).data('itemid');
      var itemqty = $(this).data('itemqty');
      var item_price = $(this).data('price');
      // var item_time = $(this).data('time');
      var refund_price = item_price * itemqty; 

      jQuery('.date_specific_cancel_'+item_id).hide();
      jQuery('.date_specific_refund_'+item_id).show();
      
      order_item_refund_date_specific(item_id,item_date);
      order_item_refund_cancel_date_specific(item_id,item_date);

      jQuery('.ptdt-setting-loader_'+item_date+'_'+item_id).show();
      var aj_nonce = admin_ajax_script.order_status_canceled_nonce;

      jQuery.ajax({
        url   : ajaxurl,
        type  : 'post',
        data  : {
          action    : 'order_status_deleted',
          item_id   : item_id,
          item_date : item_date,
          itemqty   : itemqty,
          item_price: item_price,
          nonce : aj_nonce
          //item_time : item_time,
          // nonce : aj_nonce
        },

        success : function( response ) {
          jQuery('.plan_delivery_dls_tr_'+item_date+'_'+item_id).hide();
          jQuery('.ptdt-setting-loader_'+item_date+'_'+item_id).hide();
          jQuery('.ptdt_cancelled_order_empty_note_'+item_id).hide();
          $('.ptdt_delivery_status_'+item_date+'_'+item_id).html('Pending').addClass('status_orange');
          $('.delivery_cancelled_'+item_date+'_'+item_id).html('-').addClass('not-active');
          $('.delivery_completed_'+item_date+'_'+item_id).html('-').addClass('not-active');
          jQuery('.ptdt_cancelled_items_'+item_id).show();
          jQuery('.date_specific_cancel_'+item_id).hide();
         // jQuery('.ptdt_cancelled_items_'+item_id+' .date_specific_cancel').css('display':'none');
          jQuery('.ptdt_cancelled_items_'+item_id+' table tbody').append(response);
        }
      });
    //}
  });
}

// Order Completed ajax with specific date.
function order_item_completed_date_specific(){
  $(document).on( 'click', '.ptdt_delivery_completed', function(event){
    event.preventDefault();
    var item_date = $(this).data('itemdate');
    var item_id = $(this).data('itemid');
    jQuery('.ptdt-setting-loader_'+item_date+'_'+item_id).show();
    var aj_nonce = admin_ajax_script.order_status_completed_nonce;
    jQuery.ajax({
      url : ajaxurl,
      type : 'post',
      data : {
        action : 'order_status_completed',
        item_id : item_id,
        item_date : item_date,
        nonce : aj_nonce,
      },
      success : function( response ) {
        jQuery('.ptdt-setting-loader_'+item_date+'_'+item_id).hide();
        $('.ptdt_delivery_status_'+item_date+'_'+item_id).html('Completed').addClass('status_green');
        $('.delivery_completed_'+item_date+'_'+item_id).html('-').addClass('not-active');
        $('.delivery_cancelled_'+item_date+'_'+item_id).html('-').addClass('not-active');
      }
    });
  });
}

function order_item_refund_date_specific(itemid,itemdate){
  $(document).on( 'click', '.button.date_specific_refund', function(event){
    event.preventDefault();
    var item_total = 0;
    var item_date = '';
    var item_qty = '';
    var item_id = '';
    item_date_arr = [];

    if(itemid == ''){
      item_id = $(this).parent().find('table.delivery_cancelled_item_table .sd_order_item_id').val();
    } else {
      item_id = itemid;
    }
    /*mode2*/
    // $('.delivery_cancelled_item_table .ptdt_cancelled_item_table_td.item_qty_'+item_id).children("input").each(function(){
    //   item_total += parseInt($(this).val());
    // });

    /*mode1*/

      $(this).parent().find('table.delivery_cancelled_item_table th input.order_checkbox_display.cb-select-'+item_id).each(function(){
        
        if($(this).is(":checked")){
          item_total += parseInt($(this).data('qty'));
          item_qty = $(this).data('qty');
          item_date = $(this).data('date');
          // item_time = $(this).data('time');

          item_date_arr.push($(this).data('date'));
          
        }
      });
    var aj_nonce = admin_ajax_script.order_date_specific_refund_nonce;
      jQuery.ajax({
        url   : ajaxurl,
        type  : 'post',
        data  : {
          action    : 'order_date_specific_refund',
          item_id   : item_id,
          item_date : item_date_arr,
          nonce : aj_nonce,
          // item_time : item_time,
        },

        success : function( response ) {
          $('html, body').animate({scrollTop:($('.do-manual-refund').offset().top-100)}, 500);
        }
      });

    /*end mode1*/

    
    jQuery('.date_specific_cancel_'+item_id).show();
    jQuery('.date_specific_refund_'+item_id).hide();

    $('.refund-items').trigger('click');

    //if ($('tr.item').data("order_item_id") === item_id) { 

      var refund_order_item_qty = $('tr.item').find('input[name="refund_order_item_qty[' + item_id + ']"]');
      
      $(refund_order_item_qty).val(item_total);

      refund_quantity_change(item_id);

   // }

    

  });
}


function order_item_refund_cancel_date_specific(itemid,item_date){

  $(document).on( 'click', '.button.date_specific_cancel', function(event){
    event.preventDefault();
    var item_total = 0;
    var item_date = '';
    var item_qty = '';
    var item_id = '';
    item_date_arr = [];

    if(itemid == ''){
      item_id = $(this).parent().find('table.delivery_cancelled_item_table .sd_order_item_id').val();
    } else {
      item_id = itemid;
    }
    $('.cancel-action').trigger('click');
    var refund_order_item_qty = $('tr.item').find('input[name="refund_order_item_qty[' + item_id + ']"]');
    jQuery('.date_specific_cancel_'+item_id).hide();
    jQuery('.date_specific_refund_'+item_id).show();
  });
}

function refund_quantity_change(item_id){
  var refund_qunatity     = $('input[name="refund_order_item_qty[' + item_id + ']"]');
  var $row                = refund_qunatity.closest( 'tr.item' );
  var qty                 = $row.find( 'input.quantity' ).val();
  var refund_qty          = refund_qunatity.val();
  var line_total          = $('input[name="line_total[' + item_id + ']"]', $row);
  var refund_line_total   = $('input[name="refund_line_total[' + item_id + ']"]', $row);

  // Totals
  var unit_total = accounting.unformat( line_total.attr( 'data-total' ), woocommerce_admin.mon_decimal_point ) / qty;

  refund_line_total.val(
  parseFloat( accounting.formatNumber( unit_total * refund_qty, woocommerce_admin_meta_boxes.rounding_precision, '' ) )
    .toString()
    .replace( '.', woocommerce_admin.mon_decimal_point )
  ).change();

  // Taxes
  $( '.refund_line_tax', $row ).each( function() {
    var $refund_line_total_tax = $('input[name="refund_line_tax[' + item_id + '][1]"]', $row);
    var tax_id                 = $refund_line_total_tax.data( 'tax_id' );
    var line_total_tax         = $( 'input.line_tax[data-tax_id="' + tax_id + '"]', $row );
    var unit_total_tax         = accounting.unformat( line_total_tax.data( 'total_tax' ), woocommerce_admin.mon_decimal_point ) / qty;

    if ( 0 < unit_total_tax ) {
      $refund_line_total_tax.val(
          parseFloat( accounting.formatNumber( unit_total_tax * refund_qty, woocommerce_admin_meta_boxes.rounding_precision, '' ) )
            .toString()
            .replace( '.', woocommerce_admin.mon_decimal_point )
          ).change();
    } else {
      $refund_line_total_tax.val( 0 ).change();
    }
  });

  // Restock checkbox
  if ( refund_qty > 0 ) {
    $( '#restock_refunded_items' ).closest( 'tr' ).show();
  } else {
    $( '#restock_refunded_items' ).closest( 'tr' ).hide();
    $( '.woocommerce_order_items input.refund_order_item_qty' ).each( function() {
      if ( refund_qunatity.val() > 0 ) {
        $( '#restock_refunded_items' ).closest( 'tr' ).show();
      }
    });
  }
}

  function quantity_change(){
    var quantity = $('input.quantity');
    var $row          = quantity.closest( 'tr.item' );
    var qty           = quantity.val();
    var o_qty         = quantity.attr( 'data-qty' );
    var line_total    = $( 'input.line_total', $row );
    var line_subtotal = $( 'input.line_subtotal', $row );

    // Totals
    var unit_total = accounting.unformat( line_total.attr( 'data-total' ), woocommerce_admin.mon_decimal_point ) / o_qty;
    line_total.val(
      parseFloat( accounting.formatNumber( unit_total * qty, woocommerce_admin_meta_boxes.rounding_precision, '' ) )
        .toString()
          .replace( '.', woocommerce_admin.mon_decimal_point )
      );

      var unit_subtotal = accounting.unformat( line_subtotal.attr( 'data-subtotal' ), woocommerce_admin.mon_decimal_point ) / o_qty;
      line_subtotal.val(
        parseFloat( accounting.formatNumber( unit_subtotal * qty, woocommerce_admin_meta_boxes.rounding_precision, '' ) )
          .toString()
          .replace( '.', woocommerce_admin.mon_decimal_point )
      );
  }

  function order_edit_checkbox(){
    // console.log('ghj');
    $(".ptdt-select-all").click( function() {
      $('input.order_checkbox_display:checkbox').not(this).prop('checked', this.checked);
   
    });
    $('input.order_checkbox_display').click(function(){
      var item_id = $(this).val();
      jQuery('.date_specific_cancel_'+item_id).hide();
      jQuery('.date_specific_refund_'+item_id).show();

      var chk_box_status = '';
      jQuery('input.order_checkbox_display').each(function(){
        chk_box_status = $(this).attr('checked')?true:false;
      });
    });
    
  }

  function ordered_item_delete_from_cancelled_table(){
    $(document).on( 'click', '.ptdt-dlt-fm-cancelled', function(event){
      item_id = $(this).data('item_id'); 
      item_date = $(this).data('item_date');
      item_price = $(this).data('item_price');
      // item_time = $(this).data('item_time');
      item_qty = $(this).data('item_qty');

      jQuery('.date_specific_cancel_'+item_id).hide();
      jQuery('.date_specific_refund_'+item_id).show();
      var aj_nonce = admin_ajax_script.delete_from_cancelled_table_nonce;

      jQuery.ajax({
        url   : ajaxurl,
        type  : 'post',
        data  : {
          action    : 'ordered_item_delete_from_cancelled_table',
          item_id   : item_id,
          item_date : item_date,
          item_price: item_price,
          // item_time : item_time,
          item_qty  : item_qty,
          nonce : aj_nonce,
        },

        success : function( response ) {
          $('.delivery_cancelled_tr_'+item_date+'_'+item_id).hide();
          $('.delivery_cancelled_tr_'+item_date+'_'+item_id+' .order_checkbox_display'). prop("checked", false);
          $('table.plan_delivery_dls_table_'+item_id+' tbody').append(response);
          $('ptdt_cancelled_items_'+item_id).css('display','none');
        }
      });
    });
  }

  function validate_date_format(date){
    var currVal = date;
    if(currVal == '')
        return false;
    
    // var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var rxDatePattern = /^(\d{4})(-)(\d{1,2})(-)(\d{1,2})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    
    if (dtArray == null) 
        return false;
    
    //Checks for mm/dd/yyyy format.
    dtYear = dtArray[1];
    dtMonth= dtArray[3]; 
    dtDay = dtArray[5];

    // dtMonth = dtArray[1];
    // dtDay= dtArray[3];
    // dtYear = dtArray[5];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
    }
    return true;

  }

  function validate_number_format(days){
    var intRegex = /^\d+$/;
    if(intRegex.test(days)) {
     return true;
    } else{
      return false;
    }
  }


});
