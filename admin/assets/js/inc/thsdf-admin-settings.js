/*
 * Woocommerce Product page custom tab management js.
 */

jQuery(document).ready(function(){
    var plan_delivery_tab = jQuery("#woocommerce-product-data .plan_delivery_options.plan_delivery_tab");
    var plan_delivery_checkbox = jQuery("#woocommerce-product-data .type_box input[type='checkbox']#_plan_delivery_set");
    var general_setting = jQuery(".woocommerce_options_panel .options_group input.ptdelivery-global-settings[type='checkbox']")
    
    // Plan Delivery Checkbox is already checked condition.
    if(plan_delivery_checkbox.is(":checked")){
        plan_delivery_tab.show();
        plan_delivery_tab.addClass('show_if_simple'); 
        plan_delivery_tab.addClass('show_if_variable');
    } else{
        plan_delivery_tab.hide();
        plan_delivery_tab.removeClass('show_if_simple');
        plan_delivery_tab.removeClass('show_if_variable');
    }

    // Plan Delivery Settings user global setting Checkbox is already checked condition.
    if(general_setting.is(":checked")){
        jQuery( ".thptdelivery-accordion-ul > li" ).addClass( "th-accordion-disabled" );
    } else{
        jQuery( ".thptdelivery-accordion-ul > li" ).removeClass( "th-accordion-disabled" );
    }

    // Checkbox click to enable plan delivery tab.
    // jQuery("#_plan_delivery_set").click(function () {
    //     var checked = jQuery(this).is(':checked');
    //     if (checked) {
    //         plan_delivery_tab.show();
    //         plan_delivery_tab.addClass('show_if_simple');
    //     } else {
    //         plan_delivery_tab.hide();
    //         plan_delivery_tab.removeClass('show_if_simple');
    //         jQuery('#plan_delivery_product_data').hide();
    //         plan_delivery_tab.removeClass('active');
    //         jQuery('#woocommerce-product-data .general_options.general_tab').addClass('active');
    //         jQuery('#woocommerce-product-data #general_product_data').show();
    //         jQuery('#woocommerce-product-data #license_api_product_data').hide();
    //     }
    // });
    plan_delivery_checkbox.change(function() {
        if(this.checked) {
            plan_delivery_tab.show();
            plan_delivery_tab.addClass('show_if_simple');
            plan_delivery_tab.addClass('show_if_variable');
        } else {
            // Hide tab
            jQuery('#plan_delivery_product_data').hide();
            plan_delivery_tab.hide();
            plan_delivery_tab.removeClass('show_if_simple');
            plan_delivery_tab.removeClass('show_if_variable');
            jQuery('#woocommerce-product-data .plan_delivery_options.plan_delivery_tab').removeClass('active');
            jQuery('#woocommerce-product-data .general_options.general_tab').addClass('active');
            jQuery('#woocommerce-product-data #general_product_data').show();
            // jQuery('#woocommerce-product-data #license_api_product_data').hide();
        }
    });
   
    /*
     * Accordion jquery function.
     */
    jQuery('.thptdelivery-accordion .thptdelivery-toggle').click(function(e) {
        e.preventDefault();
          
        var $this = jQuery(this);
          
        if ($this.next().hasClass('show')) {
            $this.next().removeClass('show');
            $this.next().slideUp(350);
        } else {
            $this.parent().parent().find('.thptdelivery-accordion li .thptdelivery-inner').removeClass('show');
            $this.parent().parent().find('.thptdelivery-accordion li .thptdelivery-inner').slideUp(350);
            $this.next().toggleClass('show');
            $this.next().slideToggle(350);
        }
    });

    /*
     * Enable global settings leeds to disable other options. 
     * 
     */
     var checked = jQuery(".woocommerce_options_panel .options_group input.ptdelivery-global-settings[type='checkbox']").is(':checked');
        if (checked) {
            // Disable accordion.
            jQuery( ".thptdelivery-accordion-ul > li" ).addClass( "th-accordion-disabled" );

            // Enable global settings.
            jQuery( ".woocommerce_options_panel ._global_settings_field label" ).removeClass( "th-global-settings-disabled" );
            jQuery( ".woocommerce_options_panel ._global_settings_field input.ptdelivery-global-settings" ).removeClass( "th-global-checkbox-disabled" );
        } else {
            // Enable accordion.
            jQuery( ".thptdelivery-accordion-ul > li" ).removeClass( "th-accordion-disabled" );

            // Disable global settings.
            jQuery( ".woocommerce_options_panel ._global_settings_field label" ).addClass( "th-global-settings-disabled" );
            jQuery( ".woocommerce_options_panel ._global_settings_field input.ptdelivery-global-settings" ).addClass( "th-global-checkbox-disabled" );
        }
    // jQuery( ".woocommerce_options_panel ._global_settings_field label" ).addClass( "th-global-settings-disabled" );
     jQuery(".ptdelivery-global-settings").click(function () {
        var checked = jQuery(".woocommerce_options_panel .options_group input.ptdelivery-global-settings[type='checkbox']").is(':checked');
        if (checked) {

            // Disable accordion.
            jQuery( ".thptdelivery-accordion-ul > li" ).addClass( "th-accordion-disabled" );

            // Enable global settings.
            jQuery( ".woocommerce_options_panel ._global_settings_field label" ).removeClass( "th-global-settings-disabled" );
            jQuery( ".woocommerce_options_panel ._global_settings_field input.ptdelivery-global-settings" ).removeClass( "th-global-checkbox-disabled" );

            // Disable toggle content.
            jQuery('.thptdelivery-accordion .thptdelivery-toggle').parent().parent().find('.thptdelivery-accordion li .thptdelivery-inner').removeClass('show');
            jQuery('.thptdelivery-accordion .thptdelivery-toggle').parent().parent().find('.thptdelivery-accordion li .thptdelivery-inner').slideUp(350);
            // jQuery('.thptdelivery-accordion .thptdelivery-toggle').next().toggleClass('show');
             jQuery('.thptdelivery-accordion .thptdelivery-toggle').next().slideUp(350);
        } else {

            // Enable accordion.
            jQuery( ".thptdelivery-accordion-ul > li" ).removeClass( "th-accordion-disabled" );

            // Disable global settings.
            jQuery( ".woocommerce_options_panel ._global_settings_field label" ).addClass( "th-global-settings-disabled" );
            jQuery( ".woocommerce_options_panel ._global_settings_field input.ptdelivery-global-settings" ).addClass( "th-global-checkbox-disabled" );
     
         }
    });
    
});