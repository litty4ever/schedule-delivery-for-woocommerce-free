/*
 *Product Delivery Plugin admin Js function
 *
 */
var ptdelivery_settings = (function($, window, document) {
	//jQuery( ".thwsd-action-cell.action-cell a.thwsd-remove-class" ).addClass( "unselectable" );
  	var CLASS_ROW_DEL_BTN  = '<td class="action-cell"><a href="javascript:void(0)" onclick="holidaysRemoveClassRow(this)" class="btn btn-red" title="Remove class"><span class="dashicons dashicons-no-alt"></span></a></td>';
  	var CLASS_ROW_EDIT_BTN  = '<td class="action-cell"><a href="javascript:void(0)" onclick="holidaysAddClassRow(this)" class="btn btn-green" title="Add new class"><span class="dashicons dashicons-plus"></span></a></td>';
    var CLASS_ROW_BR = '<td class="break"></td>';

	var CLASS_ROW_HTML  = '<tr>';
      	CLASS_ROW_HTML += '<td style="width:260px;"><input type="text" name="g_holidays[]" placeholder="yyyy-mm-dd" value="" class="general-set-date_holiday g-holidays" style="width:250px;" autocomplete="off" readonly /></td>';
		CLASS_ROW_HTML += CLASS_ROW_EDIT_BTN;
		CLASS_ROW_HTML += CLASS_ROW_DEL_BTN;
		CLASS_ROW_HTML += CLASS_ROW_BR
		CLASS_ROW_HTML += '</tr>';

	/*
	 * Add new holiday field.
	 */
	function addNewClassRow(elm){
		// console.log(elm);
		var ptable = $(elm).closest('table');
		// console.log(ptable);
		var rowSize = ptable.find('tbody tr').size();
		// console.log(rowSize);
		if(rowSize > 0){
			ptable.find('tbody tr:last').after(CLASS_ROW_HTML);
			// console.log(ptable);
		}else{
			ptable.find('tbody').append(CLASS_ROW_HTML);
			// console.log(ptable);
		}
		ptdt_holiday_datepicker();
	}

	/*
	 * Delete new holiday field.
	 */
	function removeClassRow(elm){
		var ptable = $(elm).closest('table');
		// console.log(ptable);
		$(elm).closest('tr').remove();
		// console.log(elm);
		var rowSize = ptable.find('tbody tr').size();
		console.log(rowSize);

		if(rowSize == 0){
			
			ptable.find('tbody').append(CLASS_ROW_HTML);
		}
		ptdt_holiday_datepicker();
	}
	/*
	 * Enable or Disable fields functions - END
	 */
	return {
	    addNewClassRow : addNewClassRow,
	    removeClassRow : removeClassRow,
   	};
}(window.jQuery, window, document));

/*
* Ready function.
*/
var holidays_data = [];
(function($){
	jQuery(document).ready(function(){
		// auto_complete_enable();
		var start_date_value = '';
		var end_date_value = '';
		var start_date_value = jQuery('#ptdt_datepicker_start').val();
		// console.log(start_date_value);
		// console.log('fdfr');
		var end_date_value = jQuery('#ptdt_datepicker_end').val();
		if(start_date_value == ''){
			// Call datepicker.
			// console.log('sdds')
			initialise_date_picker(start_date_value,end_date_value);
		}else if( (start_date_value != null) && (end_date_value == '') ){
			// Disable holiday fields and add-remove options.
			jQuery(".general-set-date_holiday").prop('disabled', true);
			jQuery( ".thsdf-action-cell.action-cell a" ).addClass( "unselectable" ); 

			// Call datepicker.
			initialise_date_picker(start_date_value,end_date_value);

			// If start date is already selected. Call end datepicker.
			initialise_end_datepicker(start_date_value);
		
		}else if( (start_date_value != null) && (end_date_value != '') ){
			// Call datepicker for start date.
			initialise_date_picker(start_date_value,end_date_value);

			// If start date is already selected. Call end datepicker.
			initialise_end_datepicker(start_date_value);

			// If start and end date already selected. Call Holiday datepicker.
			initialise_holiday_datepicker(start_date_value,end_date_value);
		}else {
			
		} 
		// Validate start date field.
		jQuery( "#ptdt_datepicker_start" ).blur(function() {
			var datepicker_start_val = jQuery('#ptdt_datepicker_start').val();
			if(datepicker_start_val == ''){
				jQuery("#ptdt_datepicker_end").prop('disabled', true);
				jQuery(".general-set-date_holiday").prop('disabled', true);
				jQuery( ".thsdf-action-cell.action-cell a" ).addClass( "unselectable" );
				this.classList.add("error");
			}
	 	})

	 	// Validate end date field.
	 	jQuery( "#ptdt_datepicker_end" ).blur(function() {
	 		var datepicker_end_val = jQuery('#ptdt_datepicker_end').val();
			if(datepicker_end_val == ''){
				jQuery(".general-set-date_holiday").prop('disabled', true);
				jQuery( ".thsdf-action-cell.action-cell a" ).addClass( "unselectable" );
				this.classList.add("error");
				
			} else{
				
			}
		});

		$('.thsdf-schedule-setting-table .button-primary-thsdf-save-settings').on('click', function(e){
			var week_days = [];
			var datepicker_end_val = jQuery('#ptdt_datepicker_end').val();
			var datepicker_start_val = jQuery('#ptdt_datepicker_start').val();
			jQuery("input[name='g_delivery_day[]']:checked").each(function() {
		        week_days.push(this.value);
		    });

		    if(datepicker_end_val == ''){
				jQuery('.thsdf-required-mssg').css('display','block');
	   			e.preventDefault();
	   		} else{
	   			jQuery('.thsdf-required-mssg').css('display','none');
	   		}
	   		if (week_days.length === 0) {
				jQuery('.thsdf-weekday-required-mssg').css('display','block');
	   			e.preventDefault();
			} else{
				jQuery('.thsdf-weekday-required-mssg').css('display','none');
			}
			if(datepicker_start_val == ''){
				jQuery('.thsdf-startdate-required-mssg').css('display','block');
	   			e.preventDefault();
	   		} else{
	   			jQuery('.thsdf-startdate-required-mssg').css('display','none');
	   		}
	   	});
	   	jQuery( "#ptdt_datepicker_end" ).change(function() {
		var datepicker_end_val = jQuery('#ptdt_datepicker_end').val();
		if(datepicker_end_val == ''){
			jQuery('.thsdf-required-mssg').css('display','block');
		} else{
			jQuery('.thsdf-required-mssg').css('display','none');
		}
		});
		jQuery('.plan_delivery_wrapper .plan_delivery_toggle').click(function(e) {
	    e.preventDefault();
	          
	    var $this = jQuery(this);
	          
	    if ($this.next().hasClass('show')) {
	        $this.next().removeClass('show');
	        $this.next().slideUp(350);
	    } else {
	        $this.parent().parent().find('.plan_delivery_wrapper .plan_delivery_details').removeClass('show');
	        $this.parent().parent().find('.plan_delivery_wrapper .plan_delivery_details').slideUp(350);
	        $this.next().toggleClass('show');
	        $this.next().slideToggle(350);
	    }
	});

	});
})(jQuery);

/*
* Add holiday field function.
*/
function holidaysAddClassRow(elm){
	ptdelivery_settings.addNewClassRow(elm);
}

/*
* Remove holiday field function.
*/
function holidaysRemoveClassRow(elm){
	ptdelivery_settings.removeClassRow(elm);
}

/*
* Initialise Datepicker function.
*/
function initialise_date_picker(start_date_value,end_date_value){
	jQuery('#ptdt_datepicker_start').datepicker({
        dateFormat: 'yy-mm-dd',
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        inline: true,
		minDate: 0,
		onSelect: function(start_date, inst) {

			// Enable end datepicker. Remove error sign.
		 	jQuery("#ptdt_datepicker_end").prop('disabled', false);
		   	this.classList.remove("error");

		   	// Disable holiday datepicker and add-remove option. 
			jQuery('#ptdt_datepicker_end').val('');
			jQuery(".general-set-date_holiday").prop('disabled', true);
			jQuery( ".thsdf-action-cell.action-cell a" ).addClass( "unselectable" );

			// Call end datepicker.
			initialise_end_datepicker(start_date);
		}
	});
}

/*
* Datepicker function for end date.
*/
function initialise_end_datepicker(start_date) {
	jQuery( "#ptdt_datepicker_end" ).datepicker( "destroy" );
	jQuery('#ptdt_datepicker_end').datepicker({
	  	dateFormat: 'yy-mm-dd',
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        inline: true,
	  	minDate: new Date(start_date),
		onSelect: function(end_date, inst) {

			// Enable holiday fields, remove error sign and enable add-remove field option.
			jQuery(".general-set-date_holiday").prop('disabled', false);
			this.classList.remove("error");
			jQuery( ".thsdf-action-cell.action-cell a" ).removeClass( "unselectable" );

			//holidays fields set empty values.
			jQuery('.general-set-date_holiday').val('');

			// Call holiday datepicker.
		 	initialise_holiday_datepicker(start_date,end_date);		   	
		}  
	});
}

/*
* Datepicker function for holidays.
*/
function initialise_holiday_datepicker(start_date,end_date) {
	jQuery( ".general-set-date_holiday" ).datepicker( "destroy" );
	
	/*
	* Function for disable already selected dates.
	*/	
	function DisableHolidays(date) {
		var holidays = get_already_set_holiday(jQuery(this));
		var y = date.getFullYear();
		var m = ("0" + (date.getMonth() + 1)).slice(-2)
		var d = ("0" + date.getDate()).slice(-2);

		dmy = y + "-" + m + "-" + d;
	    if (jQuery.inArray(dmy, holidays) == -1) {
	        return [true, ""];
	    } else {
	        return [false, "", "Unavailable"];
	    }
	}

	jQuery('.general-set-date_holiday').datepicker({
	  	dateFormat: 'yy-mm-dd',
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        inline: true,
	  	minDate: new Date(start_date),
	  	maxDate: new Date(end_date),
	  	onSelect: function(date_selected, inst) {
	  		jQuery(".general-settings-row .general-set-date_holiday").each(function() {
				holidays_data.push(this.value);
			});
	  	},
		beforeShowDay: DisableHolidays
	});
}
/*
* Datepicker function for newly created holiday fields.
*/
function ptdt_holiday_datepicker(){
	var start_date_value = jQuery('#ptdt_datepicker_start').val();
	var end_date_value = jQuery('#ptdt_datepicker_end').val();
	if( start_date_value != null ){

		// Call end datepicker.
		initialise_end_datepicker(start_date_value);
	}
	if( end_date_value != null ){

		// Call holiday datepicker.
		initialise_holiday_datepicker(start_date_value,end_date_value);
	}
}


/*
* Function for get already selected holiday list.
*/
function get_already_set_holiday(elm){
	var holiday_date = [];
	var other_holidays = jQuery('.general-set-date_holiday').not(elm);
	other_holidays.each(function(){
		var value = jQuery(this).val();
		holiday_date.push(value);
	});

	return holiday_date;
}

