jQuery(document).ready(function($){ 
    //$('.ptdyadmin-colorpick').wpColorPicker();
    $( ".ptdt-display-admin-colorpick" ).each(function() {     	
		var value = $(this).val();
		$( this ).parent().find( '.ptdt-display-admin-colorpickpreview' ).css({ backgroundColor: value });
	});

    $('.ptdt-display-admin-colorpick').iris({
		change: function( event, ui ) {
			$( this ).parent().find( '.ptdt-display-admin-colorpickpreview' ).css({ backgroundColor: ui.color.toString() });
		},
		hide: true,
		border: true
	}).click( function() {
		$('.iris-picker').hide();
		$(this ).closest('td').find('.iris-picker').show(); 
	});

	$('body').click( function() {
		$('.iris-picker').hide();
	});

	$('.ptdt-display-admin-colorpick').click( function( event ) {
		event.stopPropagation();
	});	 

});