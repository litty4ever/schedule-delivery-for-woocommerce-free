<?php
/**
 * Admin Single Product Calendar Display Settings Functionality
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    schedule-delivery-for-woocommerce-free
 * @subpackage schedule-delivery-for-woocommerce-free/admin
 */
if(!defined('WPINC')) { 
	die; 
}

if(!class_exists('THSDF_Product_Display_Settings')) :

	/**
     * Admin product display settings class extends from admin settings.
     */ 
	class THSDF_Product_Display_Settings extends THSDF_Admin_Settings {
		protected static $_instance = null;
		private $settings_fields = NULL;
		private $cell_props = array();

		/**
         * Constructor.
         */
		public function __construct() {
			$this->cell_props = array(
				'label_cell_props' => 'class="th_display_pro"', 
				'input_cell_props' => 'style=width: 50%; class=forminp', 
				'input_width' => '250px', 'label_cell_th' => true 
			);
			$this->init_constants();
		}

		/**
         * instance.
         *
         * @return void
         */
		public static function instance() {
			if(is_null(self::$_instance)) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		/**
         * Function for call the render page.
         */
		public function render_page() {
			$this->render_content();
		}	

		/**
         * Function for initialise constants.
         */
		public function init_constants() {
			$this->settings_fields = self::get_single_display_settings_fields();
		}

		/**
		 * The display settings field details of Individual product.
		 *
		 * @return array
		 */
		public static function get_single_display_settings_fields() {
			return array(
			'product_id'        	=> array('type'=>'hidden', 'name'=>'product_id'),
			'calender_colors' => array('value'=>'Calender Colors', 'type'=>'separator', 'class' => 'ptdt-display-set-calender'),
			'primary_color'	  => array('type'=>'colorpicker', 'name'=>'primary_color', 'label'=>esc_html__('
				Day Title Background','schedule-delivery-for-woocommerce-free'), 'value'=>'#e5e5e6', 'label_class' => 'ptdt-single-colorpickpreview'),
			'secondary_color'		=> array('type'=>'colorpicker', 'name'=>'secondary_color', 'label'=>esc_html__('Title Border and Navigation Arrows','schedule-delivery-for-woocommerce-free'), 'value'=>'#c4c4c4', 'label_class' => 'ptdt-single-colorpickpreview'),
			'holiday_columns'		=> array('type'=>'colorpicker', 'name'=>'holiday_columns', 'value'=>'#f8f8f8', 'label' => esc_html__('Holiday Column Background', 'schedule-delivery-for-woocommerce-free'), 'label_class' => 'ptdt-single-colorpickpreview'),
			'tooltip_bg'			=> array('type'=>'colorpicker', 'name'=>'tooltip_bg', 'value'=>'#FBFBFB', 'label' => esc_html__('Tooltip Background', 'schedule-delivery-for-woocommerce-free'), 'label_class' => 'ptdt-single-colorpickpreview'),

			'text_colors' => array('value'=>esc_html__('Text Colors','schedule-delivery-for-woocommerce-free'), 'type'=>'separator', 'class' => esc_attr('ptdt-display-set-text'), ),
			'general_color'			=> array('type'=>'colorpicker', 'name'=>'general_color', 'label'=>esc_html__('Labels', 'schedule-delivery-for-woocommerce-free'), 'value'=>'#6d6d6d', 'hint_text' => esc_html__('All labels on the calendar will be displayed in this color'), 'label_class' => 'ptdt-single-colorpickpreview' ),
			'input_value'			=> array('type'=>'colorpicker', 'name'=>'input_value', 'label'=>esc_html__('Input Text', 'schedule-delivery-for-woocommerce-free'), 'value'=>'#43454b','label_class' => 'ptdt-single-colorpickpreview' ),
			'tooltip' => array('type'=>'colorpicker', 'name'=>'tooltip', 'label'=>esc_html__('Tooltip Text', 'schedule-delivery-for-woocommerce-free'), 'value'=>'#444444','label_class' => 'ptdt-single-colorpickpreview' ),
			);
		}

		/**
		 * Function for set display settings field content.
		 */
		public function render_content() {
			$fields = $this->get_single_display_settings_fields();
			// $settings = $this->get_settings();
			// $post_id = get_the_ID();
			$product_id = $fields['product_id'];
			$primary_color = $fields['primary_color'];
			$secondary_color = $fields['secondary_color'];
			$holiday_columns = $fields['holiday_columns'];
			$tooltip_bg = $fields['tooltip_bg'];
			$general_color = $fields['general_color'];
			$input_value = $fields['input_value'];
			$tooltip = $fields['tooltip'];

			$product_id = $this->set_values_props($product_id,'product_id');
			$primary_color = $this->set_values_props($primary_color,'primary_color');
			$secondary_color = $this->set_values_props($secondary_color,'secondary_color');
			$holiday_columns = $this->set_values_props($holiday_columns,'holiday_columns');
			$tooltip_bg = $this->set_values_props($tooltip_bg,'tooltip_bg');
			$general_color = $this->set_values_props($general_color,'general_color');
			$input_value = $this->set_values_props($input_value,'input_value');
			$tooltip = $this->set_values_props($tooltip,'tooltip');
			
			?>
			<div class="wrap woocommerce">
				<div class="thsdf_display_updated_mssg"></div>
				<table class="form-table ptdt-single-display-form-table">
					<tr class= ptdt-single-display-tr>
						<th>
							<?php $this->render_form_section_separator($fields['calender_colors']); ?>
						</th>
					</tr>
					
					<?php $this->render_display_settings_elm_row($product_id,false); ?>
					<?php $this->render_display_settings_elm_row($primary_color); ?>
					<?php $this->render_display_settings_elm_row($secondary_color); ?>
					<?php $this->render_display_settings_elm_row($holiday_columns); ?>
					<?php $this->render_display_settings_elm_row($tooltip_bg); ?>
					<tr class= ptdt-single-display-tr>
						<th>
							<?php $this->render_form_section_separator($fields['text_colors']); ?>
						</th>
					</tr>
						<?php $this->render_display_settings_elm_row($general_color); ?>
						<?php $this->render_display_settings_elm_row($input_value); ?>
						<?php $this->render_display_settings_elm_row($tooltip); ?>
						
					<tr class= ptdt-single-display-tr>
						<td class="ptdt-single-display-submit-td">
							<div class="submit ptdt-single-display-set-submit">
								<input type="submit" name="save_product_display_settings" class="button-primary ptdt-single-display-settings-save" value="<?php esc_html_e('Save changes', 'schedule-delivery-for-woocommerce'); ?>">
								<input type="submit" name="default_product_display_settings" class="button default-single-display-settings" value="<?php esc_html_e('Reset to Default', 'schedule-delivery-for-woocommerce'); ?>">
							</div>
						</td>
					</tr>
				</table>
				<div class="ptdt-setting-loader" style="display:none;">
				    <img src="<?php echo plugins_url('assets/img/spinner.gif', __FILE__); ?>" />
				</div>
			</div>
  <?php }

  		/**
        *Set element row
        *
        */
        public function render_display_settings_elm_row($field,$class=true){
        	if($class == true){
	        		?>
	        	<tr class= ptdt-single-display-tr>
					<?php $this->render_form_field_element($field, $this->cell_props); ?>
				</tr>
					<?php
			}else{
				?>
				<tr style="display: none;">
					<?php $this->render_form_field_element($field, $this->cell_props); ?>
				</tr>
				<?php
			}
        }

        /**
         * Set values props.
         *
         * @param array $settings_props
         * @param string $type
         *
         * @return array
         */
        public function set_values_props($settings_props, $type) {
            if(!empty($settings_props) && is_array($settings_props)) {
            	$product_id = get_the_ID();
                $settings_props['value']=THSDF_Utils::get_setting_value($type,'product_display_settings',$product_id);
            }
            return $settings_props;
        }	

		
	}
endif;