<?php
/**
 * Admin Single Product Calendar General Settings Functionality
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    schedule-delivery-for-woocommerce-free
 * @subpackage schedule-delivery-for-woocommerce-free/admin
 */
if(!defined('WPINC')) { 
	die; 
}

if(!class_exists('THSDF_Product_General_Settings')) :

	/**
     * Admin product general settings class extends from admin settings.
     */ 
	class THSDF_Product_General_Settings extends THSDF_Admin_Settings{
		protected static $_instance = null;
		private $settings_fields = NULL;

		/**
         * Constructor.
         */
		public function __construct() {
			$this->cell_props = array(
				'label_cell_props' => '', 
				'input_cell_props' => '', 
				 'label_cell_th' => true 
			);
			$this->init_constants();
			//add_action('init', array($this, 'define_public_hooks'));
		}

		/**
         * instance.
         *
         * @return void
         */
		public static function instance() {
			if(is_null(self::$_instance)) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		/**
         * Function for call the render page.
         */
		public function render_page() {
			$this->render_content();
		}

		/**
         * Function for initialise constants.
         */
		public function init_constants() {
			$this->settings_fields = $this->get_thsdf_fields();
		}

		/**
		 * The calendar settings field details of Individual product.
		 *
		 * @return array
		 */
		public function get_thsdf_fields() {
			$week_start = $this->get_week_start();
			$delivery_day = $this->get_delivery_day();
			// THSDF_Utils::write_log($delivery_day);
			$plan_display = $this->get_plan_display();
			return array(
				'product_id'        	    => array('type'=>'hidden', 'name'=>'product_id', 'label'=>'Product Id'),
				'week_start'				=> array('type'=>'select', 'name'=>'week_start', 'label'=>'Start the Week on', 'options'=>$week_start, 'class' => esc_attr('week_start')),
				'delivery_day'  			=> array('type'=>'multicheckbox', 'name'=>'delivery_day[]', 'label'=>'Enable Plan Delivery for Weekdays', 'check'=>$delivery_day, 'required' =>1, 'id' => 'delivery_day','required_tag' => esc_html__('Weekdays are required', 'schedule-delivery-for-woocommerce-free'), 'required_class' => esc_attr('thsdf-weekday-required-mssg'),'label_class' => esc_attr('thpt-delivery-day-label'), 'class'=>'delivery_day'),
				'start_date'   				=> array('type'=>'text', 'name'=>'start_date', 'label'=>'Start Date', 'required'=>1,'id'=>'ptdt_datepicker_start','placeholder' => 'yyyy-mm-dd', 'class' => esc_attr('general-set-date general-start-date'), 'required_class' => ('thsdf-startdate-required-mssg thsdf-single-required-mssg'), 'required_tag' => esc_html__('Start Date field is required', 'schedule-delivery-for-woocommerce-free')),
				
				'end_date' 					=> array('type'=>'text', 'name'=>'end_date', 'label'=>'End Date', 'required'=>1,'id'=>'ptdt_datepicker_end','placeholder' => 'yyyy-mm-dd', 'class' => esc_attr('general-set-date'),'required_class' => ('thsdf-required-mssg thsdf-single-required-mssg'), 'required_tag' => esc_html__('End Date field is required', 'schedule-delivery-for-woocommerce-free')),
				'holidays'   				=> array('type'=>'multidatepicker', 'name'=>'holidays[]', 'label'=>'Holiday / Unavailable days', 'placeholder' => 'yyyy-mm-dd', 'class' => esc_attr('general-set-date_holiday g-holidays')),
				
				'plan_display'  			=> array('type'=>'multicheckbox', 'name'=>'plan_display[]', 'check'=>$plan_display, 'checked'=>0, 'status'=>1,'label_class' =>esc_attr('plan-display-label plan-display-pro'), 'class' => 'g-plan-display'),
				'delivery_plan_display' => array('type'=>'separator', 'value'=>esc_html__('Delivery Plan Display', 'schedule-delivery-for-woocommerce-free'), 'class' => esc_attr('thpt-general-settings-header')),
				
			);
		}

		/**
		 * Function for set Calender settings field content.
		 */
		public function render_content() {
			$fields=$this->get_thsdf_fields();
			$product_id = get_the_ID();
			$current_date = date("Y-m-d");
			$week_start = $fields['week_start'];
			$delivery_day = $fields['delivery_day'];
			$start_date = $fields['start_date'];
			$end_date = $fields['end_date'];
			$holidays = $fields['holidays'];
			$plan_display = $fields['plan_display'];

			$week_start = $this->set_values_props($week_start,'week_start');
			$delivery_day = $this->set_values_props($delivery_day,'delivery_day');
			$start_date = $this->set_values_props($start_date,'start_date');
			$end_date = $this->set_values_props($end_date,'end_date');
			$holidays = $this->set_values_props($holidays,'holidays');
			$plan_display = $this->set_values_props($plan_display,'plan_display');

		 ?>

			<div class="wrap woocommerce">
				<div class="thsdf_updated_mssg"></div>
				<!-- <form name="thsdf_product_general_settings_form" id="thsdf_product_general_settings_form" class="product-general-settings-form ptdelivery-admin-settings-form" onsubmit="return"> -->
					<?php 
					// if (function_exists('wp_nonce_field')) {
	                        // wp_nonce_field('product_general_settings_form', 'thsdf_product_general_settings_form'); 
	                    // }
	                     ?>
					<table class="form-table thpt-form-table thsdf-schedule-setting-table" >
						<?php $product_id = get_the_ID(); ?>
						<input type="hidden" name="g_product_id" value="<?php echo $product_id; ?>">
						<?php $this->render_general_settings_elm_row($week_start); ?>
						<?php $this->render_general_settings_elm_row($delivery_day); ?>
						<?php $this->render_general_settings_elm_row($start_date); ?>
						<?php $this->render_general_settings_elm_row($end_date); ?>
						<?php $this->render_general_settings_elm_row($holidays); ?>
						
					</table>

					

					<?php $this->render_form_section_separator($fields['delivery_plan_display']); ?>
					<table class="thpt-form-table thsdf-schedule-setting-table">
						<?php $this->render_general_settings_elm_row($plan_display,false); ?>
						
						<tr class="thpt-general-settings-row thpt-general-settings-submit">
							<td class="thsdf-single-submit-td">
								<div class="submit">
									<input type="submit" name="save_calender_settings" class="button-primary save_calender_settings button-primary-thsdf-save-settings" value="<?php esc_html_e('Save changes', 'schedule-delivery-for-woocommerce'); ?>">
									<input type="submit" name="default_general_settings" class="button default-single-general-settings" value="<?php esc_html_e('Reset to Default', 'schedule-delivery-for-woocommerce'); ?>" >
				              	</div>
							</td>
						</tr>
					</table>

					<div class="ptdt-setting-loader" style="display:none;">
					    <img src="<?php echo esc_url_raw(plugins_url('assets/img/spinner.gif', __FILE__)); ?>" />
					</div>
				<!-- </form> -->
			</div>
		<?php }

		/**
        *Set element row
        *
        */
        public function render_general_settings_elm_row($field,$class=true){
        	if($class == true){
	        	?>
	        	<tr class="thpt-general-settings-row">
					<?php $this->render_form_field_element($field, $this->cell_props); ?>
				</tr><?php
			}else{
				?>
	        	<tr>
					<?php $this->render_form_field_element($field, $this->cell_props); ?>
				</tr><?php
			}
        }
        /**
         * Set values props.
         *
         * @param array $settings_props
         * @param string $type
         *
         * @return array
         */
        public function set_values_props($settings_props, $type) {
            if(!empty($settings_props) && is_array($settings_props)) {
            	$product_id = get_the_ID();
                $settings_props['value']=THSDF_Utils::get_setting_value($type,'product_general_settings',$product_id);
            }
            return $settings_props;
        }		
	}
endif;