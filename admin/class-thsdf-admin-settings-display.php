<?php
/**
 * The admin display settings page functionality of the plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    schedule-delivery-for-woocommerce-free
 * @subpackage schedule-delivery-for-woocommerce-free/admin
 */
if(!defined('WPINC')) { 
	die; 
}

if(!class_exists('THSDF_Admin_Settings_Display')) :

	/**
     * The admin display settings class extends from admin settings class.
     */
	class THSDF_Admin_Settings_Display extends THSDF_Admin_Settings {
		protected static $_instance = null;
		private $settings_fields = NULL;
		private $cell_props = array();

		/**
         * Constructor.
         */
		public function __construct() {
			parent::__construct('display_settings');
			$this->cell_props = array(
				'label_cell_props' => 'class="th_display"', 
				'input_cell_props' => 'style="width: 25%;" class="forminp"', 
				'input_width' => '250px', 'label_cell_th' => true 
			);
			$this->init_constants();
		}

		/**
         * Function for instance.
         *
         * @return string
         */
		public static function instance() {
			if(is_null(self::$_instance)) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		/**
         * Function for render page.
         */
		public function render_page() {
			$this->render_tabs();
			$this->render_content();
		}
		/**
         * Function for initialise constants.
         */
		public function init_constants() {
			$this->settings_fields = self::get_field_form_props();
		}

		/**
         * Function for get display settings fields.
         */
		public static function get_field_form_props() {

			return array(
				'calender_colors' => array('value'=>'Calender Colors', 'type'=>'separator', 'class' => 'ptdt-display-set-calendar'),
				'primary_color'	  => array('type'=>'colorpicker', 'name'=>'primary_color', 'label'=>esc_html__('
					Day Title Background','schedule-delivery-for-woocommerce-free'), 'value'=>'#e5e5e6'),
				'secondary_color'		=> array('type'=>'colorpicker', 'name'=>'secondary_color', 'label'=>esc_html__('Title Border and Navigation Arrows','schedule-delivery-for-woocommerce-free'), 'value'=>'#c4c4c4'),
				'holiday_columns'		=> array('type'=>'colorpicker', 'name'=>'holiday_columns', 'value'=>'#f8f8f8', 'label' => esc_html__('Holiday Column Background', 'schedule-delivery-for-woocommerce-free')),
				'tooltip_bg'			=> array('type'=>'colorpicker', 'name'=>'tooltip_bg', 'value'=>'#FBFBFB', 'label' => esc_html__('Tooltip Background', 'schedule-delivery-for-woocommerce-free')),

				'text_colors' => array('value'=>esc_html__('Text Colors','schedule-delivery-for-woocommerce-free'), 'type'=>'separator', 'class' => esc_attr('ptdt-display-set-text')),
				'general_color'			=> array('type'=>'colorpicker', 'name'=>'general_color', 'label'=>esc_html__('Labels', 'schedule-delivery-for-woocommerce-free'), 'value'=>'#6d6d6d', 'hint_text' => esc_html__('All labels on the calendar will be displayed in this color') ),
				'input_value'			=> array('type'=>'colorpicker', 'name'=>'input_value', 'label'=>esc_html__('Input Text', 'schedule-delivery-for-woocommerce-free'), 'value'=>'#43454b' ),
				'tooltip' => array('type'=>'colorpicker', 'name'=>'tooltip', 'label'=>esc_html__('Tooltip Text', 'schedule-delivery-for-woocommerce-free'), 'value'=>'#444444', ),

			);
		}

		/**
         * Function for render content.
         */
		public function render_content() {
			if(isset($_POST['save_display_settings'])) {
				// THSDF_Utils::write_log('dfvgb');
				$this->save_display_settings();
			}
			if(isset($_POST['default_display_settings'])){
				$this->default_display_settings();
			}
			$save_display_settings = get_option(THSDF_Utils::OPTION_KEY_DISPLAY_SETTINGS);
			$fields = $this->get_field_form_props();
			$primary_color = $fields['primary_color'];
			$secondary_color = $fields['secondary_color'];
			$holiday_columns = $fields['holiday_columns'];
			$tooltip_bg = $fields['tooltip_bg'];
			$general_color = $fields['general_color'];
			$input_value = $fields['input_value'];
			$tooltip = $fields['tooltip'];

			$primary_color = $this->set_values_props($primary_color,'primary_color');
			$secondary_color = $this->set_values_props($secondary_color,'secondary_color');
			$holiday_columns = $this->set_values_props($holiday_columns,'holiday_columns');
			$tooltip_bg = $this->set_values_props($tooltip_bg,'tooltip_bg');
			$general_color = $this->set_values_props($general_color,'general_color');
			$input_value = $this->set_values_props($input_value,'input_value');
			$tooltip = $this->set_values_props($tooltip,'tooltip');
            ?>
            <div class="wrap woocommerce">
				<form action="" method="post" name="thsdf_display_settings_form" class="ptdt-display-settings-form">
					<?php if (function_exists('wp_nonce_field')) {
                        wp_nonce_field('display_settings_form', 'display-settings-form'); 
                    } ?>
						<?php $this->render_form_section_separator($fields['calender_colors']); ?>
					<table class="form-table ptdt-display-admin-form-table">
						<!-- <tr></tr> -->
						<?php $this->render_display_settings_elm_row($primary_color); ?>
						<?php $this->render_display_settings_elm_row($secondary_color); ?>
						<?php $this->render_display_settings_elm_row($holiday_columns); ?>
						<?php $this->render_display_settings_elm_row($tooltip_bg); ?>
					</table>
					<?php $this->render_form_section_separator($fields['text_colors']); ?>
					<table class="form-table ptdt-display-admin-form-table">
						<!-- <tr></tr> -->
						<?php $this->render_display_settings_elm_row($general_color); ?>
						<?php $this->render_display_settings_elm_row($input_value); ?>
						<?php $this->render_display_settings_elm_row($tooltip); ?>
						<tr class="general-settings-row">
							<th class="submit">
								<input type="submit" name="save_display_settings" class="button-primary ptdt-general-display-settings-save" value="<?php esc_html_e('Save changes', 'schedule-delivery-for-woocommerce'); ?>">
								<input type="submit" name="default_display_settings" class="button default-display-settings" value="<?php esc_html_e('Reset to Default', 'schedule-delivery-for-woocommerce'); ?>" onclick="return confirm('Are you sure you want to reset to default settings? all your changes will be deleted.');">
				            </th>
						</tr>
					</table>
				</form>
			</div>
		<?php
            

		}

		/**
         * Function for save display settings.
         *
         * @return void
         */
		function save_display_settings() {
			if(wp_verify_nonce($_POST['display-settings-form'],'display_settings_form') && isset($_POST['display-settings-form'])) {
				$display_settings = array();
				if(!empty($this->settings_fields) && is_array($this->settings_fields)) {
					foreach($this->settings_fields as $name => $field) {
						$value = '';
						if($field['type'] === 'checkbox') {
							$value = (isset($_POST['g_'.$name]) && !empty($_POST['g_'.$name]))  ? sanitize_text_field($_POST['g_'.$name]) : '';
						}else if($field['type'] === 'multiselect_grouped') {
							$value = (isset($_POST['g_'.$name]) && !empty($_POST['g_'.$name])) ? sanitize_text_field($_POST['g_'.$name]) : '';
							$value = is_array($value) ? implode(',', $value) : $value;
						}else if($field['type'] === 'text' || $field['type'] === 'textarea') {
							$value = (isset($_POST['g_'.$name]) && !empty($_POST['g_'.$name])) ? sanitize_text_field($_POST['g_'.$name]) : '';
							$value = !empty($value) ? stripslashes(trim($value)) : '';
						}else {
							$value = (isset($_POST['g_'.$name]) && !empty($_POST['g_'.$name])) ? ($_POST['g_'.$name]) : '';
							$value = is_array($value) ? array_map('sanitize_text_field',$value) : sanitize_text_field($value);
						}
						if(!empty($value)) {
							$value = preg_replace('/\s/', '', $value);
						}
						$settings[$name] = $value; 
					}
				}

				// Update option data of general settings.
				$result = false;
				if(!empty($settings)) {
					$result = update_option(THSDF_Utils::OPTION_KEY_DISPLAY_SETTINGS, $settings);
				}
				if($result == true) {
					parent::print_notices('Your changes were saved','updated',false);
				} else {
					parent::print_notices('Your changes were not saved due to an error (or you made none!)','error',false);
				}
			}
		}

		/**
         * Function for update default settings and set reset message.
         *
         */
		function default_display_settings() {
			if(wp_verify_nonce($_POST['display-settings-form'],'display_settings_form') && isset($_POST['display-settings-form'])) {
				$settings = '';

				// Update option data of general settings.
				parent::print_notices('Settings successfully reset.','updated',false);
				$result = delete_option(THSDF_Utils::OPTION_KEY_DISPLAY_SETTINGS, $settings);
			}
		}

		/**
         * Set values props.
         *
         * @param array $settings_props
         * @param string $type
         *
         * @return array
         */
        public function set_values_props($settings_props, $type) {
        	
            if(!empty($settings_props) && is_array($settings_props)) {
                
                $settings_props['value']=THSDF_Utils::get_setting_value($type,'display_settings');
            }
            return $settings_props;
        }

        /**
        *Set element row
        *
        */
        public function render_display_settings_elm_row($field){
        	?>
        	<tr>
				<?php $this->render_form_field_element($field, $this->cell_props); ?>
			</tr><?php
        }

		
	}
endif;