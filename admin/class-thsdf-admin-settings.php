<?php
/**
 * The admin settings page specific functionality of the plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    schedule-delivery-for-woocommerce-free
 * @subpackage wschedule-delivery-for-woocommerce-free/admin
 */
if(!defined('WPINC')) { 
	die; 
}

if(!class_exists('THSDF_Admin_Settings')) :
	// THSDF_Utils::write_log('dgf');
	/**
     * Admin settings class.
    */ 
	abstract class THSDF_Admin_Settings {
		protected $page_id = '';	
		protected $tabs = '';

		// $this->get_tabs();
		/**
         * Constructor function.
         *
         * @param string $page The page id info
         * @param string $section The section name info
         */
		public function __construct($page, $section = '') {
			// THSDF_Utils::write_log($page);
			$this->page_id = $page;
			$this->tabs = array('general_settings' => 'General Settings', 'display_settings' => 'Display Settings');
			add_action('init', array($this, 'write_log'));
		}

		/**
         * Function for get tab.
         */
		public function get_tabs() {
			// write_log('dsd');
			return $this->tabs;
		}

		/**
         * Function for get current tab.
         */
		public function get_current_tab() {
			return $this->page_id;
		}

		/**
         * Function for get render tab.
         */
		public function render_tabs() {
			$current_tab = $this->get_current_tab();
			// THSDF_Utils::write_log($current_tab);
			$tabs = $this->get_tabs();
			if(empty($tabs)) {
				return;
			}			
			echo '<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">';
			if(!empty($tabs) && is_array($tabs)){
				foreach($tabs as $id => $label) {
					$active = ($current_tab == $id) ? 'nav-tab-active' : '';
					$label = __($label, 'schedule-delivery-for-woocommerce-free');
					echo '<a class="nav-tab ' . esc_attr__($active).'" href="'. ($this->get_admin_url($id)) .'">'.esc_attr__($label).'</a>';
				}
			}
			echo '</h2>';		
		}
	
		/**
         * Function for get admin url.
         *
         * @param string $tab The tab name on delivery settings section
         *
         * @return string
         */
		public function get_admin_url($tab = false) {
			$url = 'edit.php?post_type=product&page=th_schedule_delivery_plan';
			if($tab && !empty($tab)) {
				$url .= '&tab='. $tab;
			}
			return admin_url($url);
		}

		/**
         * Function for get admin url.
         *
         * @param string $tooltip The tooltip info
         *
         * 
         */
		public function render_form_fragment_tooltip($tooltip = false) {
			if($tooltip) { 
				$tooltip = '<span class="thsdf_tooltip_data"><a href="javascript:void(0)" title="' . esc_html__($tooltip, "schedule-delivery-for-woocommerce-free") .'" class="thsdf_tooltip"><img src="' . THSDF_ASSETS_URL_ADMIN . '/img/help.png" title=""/></a></span>'; 
				return $tooltip;
			} else { ?>
				<!-- <td style="width: 26px; padding:0px;"></td> -->

			<?php }
		}
        /**
         * Function for display the notices.
         *
         * @param $msg The message
         * @param $class_name The class  name
         * 
         * @return notice
         */
		public static function print_notices($msg, $class_name='updated',$return=true){
			$notice = '<div class="'. $class_name .'"><p>'. esc_html__($msg, 'schedule-delivery-for-woocommerce-free') .'</p></div>';
			if($return == false){
				echo $notice;
			}
			return $notice;
		}
        /**
         * Function for getting the week start.
         * 
         * 
         */
        public function get_week_start(){
          return  array(
                            'sunday' => esc_html__('Sunday', 'schedule-delivery-for-woocommerce-free'),
                            'monday' => esc_html__('Monday', 'schedule-delivery-for-woocommerce-free'),
                            'tuesday' => esc_html__('Tuesday', 'schedule-delivery-for-woocommerce-free'),
                            'wednesdy' => esc_html__('Wednesday', 'schedule-delivery-for-woocommerce-free'),
                            'thursday' => esc_html__('Thursday', 'schedule-delivery-for-woocommerce-free'),
                            'friday' => esc_html__('Friday', 'schedule-delivery-for-woocommerce-free'),
                            'saturday' => esc_html__('Saturday', 'schedule-delivery-for-woocommerce-free')

                        );
        }
        /**
         * Function for getting get delivery day.
         * 
         * 
         */
        public function get_delivery_day(){
            return array(
                            1 => 'M',
                            2 => 'T',
                            3 => 'W',
                            4 => 'T',
                            5 => 'F',
                            6 => 'S',
                            0 => 'S'
                        );
        }
        /**
         * Function for getting plan display.
         * 
         * 
         */
        public function get_plan_display(){
            return array(
                            'cart-page-view' => esc_html__('Display Delivery Details on the Cart', 'schedule-delivery-for-woocommerce-free'),
                            'checkout-page-view' =>esc_html__('Display Delivery details on the Checkout page', 'schedule-delivery-for-woocommerce-free'),
                            'thankyou-page-view' => esc_html__('Display Delivery details on the Thank you page', 'schedule-delivery-for-woocommerce-free')

                        );
        }
		/**
         * Render form field element.
         *
         * @param array $field the field datas
         * @param array $atts field style array attribute
         * @param array $render_cell render cell information
         *
         * @return array
         */
        public function render_form_field_element($field, $atts = array(), $render_cell = true) {
        	// THSDF_Utils::write_log('dfgh');
        	if($field && is_array($field)) {
                $args = shortcode_atts(array(
                    'label_cell_props' => '',
                    'input_cell_props' => '',
                    'label_cell_colspan' => '',
                    'input_cell_colspan' => '',
                ), $atts);
                // THSDF_Utils::write_log('dfgh');
                $ftype     = isset($field['type']) ? $field['type'] : 'text';
                $flabel    = isset($field['label']) && !empty($field['label']) ? THSDF_i18n::__t($field['label']) : '';
                $sub_label = isset($field['sub_label']) && !empty($field['sub_label']) ? $field['sub_label'] : '';
                $tooltip   = isset($field['hint_text']) && !empty($field['hint_text']) ? ($field['hint_text']) : '';
                
                $field_html = '';
                
                if($ftype == 'text') {
                    $field_html = $this->_render_form_field_element_inputtext($field, $atts);                   
                }
                else if($ftype == 'number') {
                    $field_html = $this->_render_form_field_element_inputnumber($field, $atts);
                    
                }else if($ftype == 'textarea') {
                    $field_html = $this->_render_form_field_element_textarea($field, $atts);
                       
                }else if($ftype == 'select') {
                    // THSDF_Utils::write_log('dfgh');
                    $field_html = $this->_render_form_field_element_select($field, $atts);     
                    
                }else if($ftype == 'multicheckbox') {
                    $field_html = $this->_render_form_field_element_multicheckbox($field, $atts);     
                    
                }else if($ftype == 'colorpicker') {
                    $field_html = $this->_render_form_field_element_colorpicker($field, $atts);              
                
                }else if($ftype == 'checkbox') {
                    $field_html = $this->_render_form_field_element_checkbox($field, $atts, $render_cell);   
                    $flabel     = '&nbsp;';  
                }else if($ftype == 'multidatepicker') {
                    // THSDF_Utils::write_log('dfgh');
                    $field_html = $this->_render_form_field_element_multidatepicker($field, $atts, $render_cell);   
                    // $flabel     = '&nbsp;';  
                }else if($ftype == 'hidden') {
                    // THSDF_Utils::write_log('dfgh');
                    $field_html = $this->_render_form_field_element_hidden($field, $atts);   
                    // $flabel     = '&nbsp;';  
                }
                
                if($render_cell) {

                // THSDF_Utils::write_log($ftype);
                    $required_html = isset($field['required']) && $field['required'] ? '<span class="thsdf-required">*<span>' : '';
                    $required_tag = (!empty($required_html) && isset($field['required_tag']) && isset($field['required_class'])) ? '<span class="'.esc_attr($field['required_class']).'"><p>'. $field['required_tag'] .'</p></span>' : '';
                    // THSDF_Utils::write_log($required_tag);
                    $label_cell_props = !empty($args['label_cell_props']) ? $args['label_cell_props'] : '';
                    $input_cell_props = (!empty($args['input_cell_props'])) ? $args['input_cell_props'] : '';
                    // $input = ($ftype != 'multicheckbox') ? $args['input_cell_props'] : '';
                // THSDF_Utils::write_log($input_cell_props);

                // THSDF_Utils::write_log($label_cell_props);
                    if($flabel){ 
                        if($tooltip){
                            $tooltip = $this->render_form_fragment_tooltip($tooltip);
                            ?>
                            <th <?php echo($label_cell_props) ?>><?php esc_html_e($flabel,'schedule-delivery-for-woocommerce-free'); echo ($required_html);?><?php echo $tooltip; ?></th>
                            <td <?php esc_html_e($input_cell_props) ?> ><?php echo $field_html;?><?php echo $required_tag; ?> </td>
                        <?php
                        }else{
                            // THSDF_Utils::write_log($flabel);
                        ?>
                            <th <?php echo($label_cell_props) ?>><?php  esc_html_e($flabel,'schedule-delivery-for-woocommerce-free'); echo ($required_html); ?></th>
                            <?php
                            $input_cell_props = (($ftype == 'multicheckbox') && !empty($input_cell_props)) ? ('class="ptdt_general_settings_td ptdt_general_weekdays_td"') : esc_html__($input_cell_props);
                            // THSDF_Utils::write_log($input_cell_props);
                            ?>
                            <td <?php echo($input_cell_props) ?> ><?php echo $field_html;?><?php echo $required_tag; ?> </td>
                        <?php
                        }
                    }else{
                    ?>
                    <td <?php esc_html_e($input_cell_props) ?> ><?php echo $field_html; ?><?php echo $required_tag; ?> </td>
                <?php } 
                }else {

                // THSDF_Utils::write_log($field_html);
                    echo $field_html;
                }
            }

        }

        /**
         * Function prepare field props.
         *
         * @param array $field the field data
         * @param array $atts the input info
         *
         * @return array
         */
        private function _prepare_form_field_props($field, $atts = array()) {
            $field_props = '';
            $args = shortcode_atts(array(
                'input_width' => '',
                'input_name_prefix' => 'g_',
                'input_name_suffix' => '',
            ), $atts);
            
            $ftype = isset($field['type']) ? $field['type'] : 'text';
            
            $fname  = $args['input_name_prefix'].$field['name'].$args['input_name_suffix'];
            // THSDF_Utils::write_log($fname);
            $fvalue = (isset($field['value']) && !is_array($field['value']) && !empty($field['value'])) ? ' value=' . esc_attr__($field['value']) . '' : '';
            // THSDF_Utils::write_log($fvalue);
            $input_width  = $args['input_width'] && ($ftype != 'multicheckbox') ? 'width:'.$args['input_width'].';' : ''; 
            // THSDF_Utils::write_log($input_width);
            $style = isset($input_width) && !empty($input_width) ? ' style="'. esc_attr__($input_width) . '"' : '';
            $class_name = isset($field['class']) && !empty($field['class']) ? ' class="'.esc_attr__($field['class']).'"' : '';
            $fid = isset($field['id']) && !empty($field['id']) ? 'id="'.esc_attr__($field['id']).'"' : '';
            $field_props  = 'name="'. esc_attr__($fname) . '" ' . esc_attr__($fvalue) . $style . $class_name . $fid ;
            $field_props .= (isset($field['placeholder']) && !empty($field['placeholder'])) ? ' placeholder="'.esc_attr__($field['placeholder']).'"' : '';
            $field_props .= (isset($field['onchange']) && !empty($field['onchange'])) ? ' onchange="'.$field['onchange'].'"' : '';
            if($ftype == 'number') {
                $fmin=isset($field['min']) ? $field['min'] : '';
                $fmax=isset($field['max']) ? $field['max'] : '';
                $field_props .= 'min="'. $fmin .'"max="'.$fmax.'"';
            }
            // THSDF_Utils::write_log($field_props);
            return $field_props;
        }
        /**
         * Render form field for text input element.
         *
         * @param string $field the field data
         * @param string $atts the attribute information
         *
         * @return void
         */
        private function _render_form_field_element_inputtext($field, $atts = array()) {
            $field_html = '';
            if($field && is_array($field)) {
                $field_props = $this->_prepare_form_field_props($field, $atts);
                $field_html = '<input type="text" '. $field_props .' autocomplete="off" readonly />';
            }
            return $field_html;
        }
        /**
         * Render form field for text input element.
         *
         * @param string $field the field data
         * @param string $atts the attribute information
         *
         * @return void
         */
        private function _render_form_field_element_hidden($field, $atts = array()) {
            $field_html = '';
            if($field && is_array($field)) {
                $field_props = $this->_prepare_form_field_props($field, $atts);
                $field_html = '<input type="hidden" '. $field_props .' autocomplete="off" readonly />';
            }
            return $field_html;
        }
        /**
         * Render form field for colorpicker element.
         *
         * @param string $field the field data
         * @param string $atts the attribute information
         *
         * @return void
         */
        private function _render_form_field_element_colorpicker($field, $atts = array()) {
            $field_html = '';
            if($field && is_array($field)) {
                $field_props = $this->_prepare_form_field_props($field, $atts);
                // THSDF_Utils::write_log(esc_attr($field_props));
                $label_class = isset($field['label_class']) && !empty($field['label_class']) ? $field['label_class'] : '';
                $field_html  .= '<span class="ptdt-display-admin-colorpickpreview ' . esc_attr__($field['name']) . '_preview '.$label_class.' " style="background:'.($field['value']).';"></span>';
		        $field_html .= '<input type="text" '. $field_props .' class="ptdt-display-admin-colorpick input" autocomplete="off"/>';
		        
            }
            return $field_html;
        }

        /**
         * Render form field for select element.
         *
         * @param string $field the field data
         * @param string $atts the attribute information
         *
         * @return void
         */
        private function _render_form_field_element_select($field, $atts = array()) {
            $field_html = '';
            if($field && is_array($field)) {
                $fvalue = isset($field['value']) ? $field['value'] : '';
                $field_props = $this->_prepare_form_field_props($field, $atts);
                
                $field_html = '<select '. $field_props .'>';
                if(!empty($field['options']) && is_array($field['options'])){
                    // THSDF_Utils::write_log($field['options']);
                    foreach($field['options'] as $value => $label){
                        $selected = $value === $fvalue ? 'selected' : '';
                        $field_html .= '<option value="'. trim($value) .'" '.$selected.'>'. THSDF_i18n::__t($label) .'</option>';
                    }
                }
                $field_html .= '</select>';
            }
            return $field_html;
        }

         /**
         * Render form field for multi select element.
         *
         * @param string $field the field data
         * @param string $atts the attribute information
         *
         * @return void
         */
        private function _render_form_field_element_multicheckbox($field, $atts = array()) {
            // $field_html = '';

            $field_html = '';
            if($field && is_array($field)) {
                $args = shortcode_atts(array(
                    'label_props' => '',
                    'cell_props'  => 3,
                    'render_input_cell' => false,
                ), $atts);
            
                $fid  =  isset($field['id']) ? 'id="'.$field['id'].'"' : '';
                $fvalue = isset($field['value']) ? $field['value'] : '';
                // THSDF_Utils::write_log($fvalue);
                $field_props  = $this->_prepare_form_field_props($field, $atts);
                // $field_props .= isset($field['checked']) && $field['checked'] === 1  ? ' checked' : '';
                
                // $field_html = '<td class="ptdt_general_settings_td ptdt_general_weekdays_td">';
                if(!empty($field['check']) && is_array($field['check']) && isset($field['check'])){
                    foreach ($field['check'] as $key => $value){

                        $checked = !empty($fvalue) && in_array($key,$fvalue) ? ' checked' : '';
                        // THSDF_Utils::write_log($checked);
                        $label = '<label class= "'.$field['label_class'].'">'.$value .'</label>';
                        $input  = '<input type="checkbox" '. $fid . $field_props .' value="'.$key.'" '.$checked.'/>';

                        if($field['name'] == 'plan_display[]'){
                            $field_html .= '<div class="plan_display_notf">' . $input . $label . '</div>';
                        }else{
                            $field_html .= $label . $input;
                        }
                    }
                }
                // $field_html .= '</td>';
            }
            return $field_html;
        }
        public function _render_form_field_element_multidatepicker($field, $atts = array()) {
            $field_html = '<table border="0" cellpadding="0" cellspacing="0" class="holidays-list ptdt-admin-dynamic-row-table"><tbody>';
            if($field && is_array($field)){
                $field_props = $this->_prepare_form_field_props($field, $atts);
                // THSDF_Utils::write_log($field['value']);
                $default = '<tr>
                                <td style="width:260px;">
                                    <input type="text" name="g_holidays[]" placeholder="yyyy-mm-dd" value="" class="general-set-date_holiday g-holidays" style="width:250px;" autocomplete="off" readonly/>
                                </td>
                                <td class="action-cell">
                                    <a href="javascript:void(0)" onclick="holidaysAddClassRow(this)" class="btn btn-green" title="Add new class"><span class="dashicons dashicons-plus"></span></a>
                                </td>
                                <td class="action-cell">
                                    <a href="javascript:void(0)" onclick="holidaysRemoveClassRow(this)" class="btn btn-red" title="Remove class"><span class="dashicons dashicons-no-alt"></span></a>
                                </td>                                       
                            </tr>';
                if(!empty($field['value'])&& is_array($field['value'])){
                    // THSDF_Utils::write_log('hiii');
                    foreach ($field['value'] as $key => $value){
                        if(!empty($value)){
                            $field_html .= '<tr>';
                            $field_html .= '<td style="width:260px;">';
                            $field_html .= '<input type="text" '. $field_props .' autocomplete="off" readonly value="'.$value.'"/>';
                            $field_html .= '</td>';
                            $field_html .= '<td class="thsdf-action-cell action-cell">
                                                <a href="javascript:void(0)" onclick="holidaysAddClassRow(this)" class="btn btn-green thsdf-add-class" title="Add new class"><span class="dashicons dashicons-plus"></span></a>
                                            </td>
                                            <td class="thsdf-action-cell action-cell">
                                                <a href="javascript:void(0)" onclick="holidaysRemoveClassRow(this)" class="btn btn-red thsdf-remove-class" title="Remove class"><span class="dashicons dashicons-no-alt"></span></a>
                                            </td>                                           
                                            <td class="break"></td>';
                            $field_html .= '</tr>';
                        } else if($key == 0){
                            // THSDF_Utils::write_log($key);
                            $field_html .= $default;
                        }

                    }
                } else{
                    // THSDF_Utils::write_log('hello');
                    $field_html .= $default;
                }
            }
            $field_html .= '</tbody></table>';
            return $field_html;
        }

		public function render_form_section_separator($props) { 
        ?>
			<h3 class='<?php  esc_attr_e($props['class']);?> '><?php esc_html_e($props['value']); ?></h3>
            
        <?php 
    	}
    	

	}
endif;
