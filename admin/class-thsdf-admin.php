<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    schedule-delivery-for-woocommerce-free
 * @subpackage schedule-delivery-for-woocommerce-free/admin
 */
if(!defined('WPINC')) { 
	die; 
}

if(!class_exists('THSDF_Admin')) :

	/**
     * Admin class.
    */
	class THSDF_Admin {
		private $plugin_name;
		private $version;

		/**
		 * Initialize the class and set its properties.
		 *
		 * @param      string    $version    The version of this plugin.
		 * @param      string    $plugin_name       The name of this plugin.
		 */
		public function __construct($version, $plugin_name) {
			$this->plugin_name = $plugin_name;
			$this->version = $version;
			// $this->define_admin_hook();
		}

		/**
		 * Function for enqueue script and style.
		 *
		 * @param      string    $hook       The screen id of the plugin.
		 *
		 * @return void
		 */
		public function enqueue_styles_and_scripts($hook) {
			if(strpos($hook, 'product_page_th_schedule_delivery_plan') === false) {
				return;
			}

			$debug_mode = apply_filters('thsdf_debug_mode', false);
			$suffix = $debug_mode ? '' : '.min';

			$this->enqueue_styles($suffix);
			$this->enqueue_scripts($suffix);
		}

		/**
		 * Function for enqueue style.
		 *
		 * @param      string    $suffix       The stylesheet suffix.
		 */
		private function enqueue_styles($suffix) {
			wp_enqueue_style('jquery-ui-style', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css?ver=1.11.4');
			wp_enqueue_style('jquery-ui');
			wp_enqueue_style('woocommerce_admin_styles', THSDF_WOO_ASSETS_URL.'css/admin.css');
			wp_enqueue_style('thsdf-admin-style', THSDF_ASSETS_URL_ADMIN . 'css/thsdf-admin'. $suffix .'.css', THSDF_VERSION);
		}

		/**
		 * Function for enqueue script.
		 *
		 * @param      string    $suffix       The js file suffix.
		 */
		private function enqueue_scripts($suffix) {
			$deps = array('jquery', 'jquery-ui-dialog', 'jquery-ui-sortable', 'jquery-tiptip', 'woocommerce_admin', 'wc-enhanced-select', 'select2', 'wp-color-picker', 'jquery-ui-datepicker'); 
			
			wp_enqueue_script('thsdf-admin-script', THSDF_ASSETS_URL_ADMIN . 'js/thsdf-admin'. $suffix .'.js', $deps, THSDF_VERSION, false);
	
			
		}
		/**
		* Function for set capability
		*/
		public function wcfd_capability() {
		$allowed = array('manage_woocommerce','manage_options');
		// THSDF_Utils::write_log($allowed);
		$capability = apply_filters('thsdf_required_capability', 'manage_woocommerce');
		// THSDF_Utils::write_log($capability);
		if(!in_array($capability, $allowed)){
			$capability = 'manage_woocommerce';
		}
		return $capability;
	}

		/**
		 * Function for creating admin menu.
		 */
		public function admin_menu() {
		$capability = $this->wcfd_capability();
		// THSDF_Utils::write_log($capability);
		$this->screen_id = add_submenu_page('edit.php?post_type=product', esc_html__('Plan Delivery', 'schedule-delivery-for-woocommerce-free'),
		esc_html__('Plan Delivery', 'plan-delivery'), $capability, 'th_schedule_delivery_plan', array($this, 'output_settings'));
		}

		/**
		 * Function for set screen id.
		 *
		 * @param      string    $ids       The screen id.
		 *
		 * @return array
		 */
		public function add_screen_id($ids) {
			$ids[] = 'woocommerce_page_th_schedule_delivery_plan';
			$ids[] = strtolower(esc_html__('WooCommerce', 'schedule-delivery-for-woocommerce-free')) .'_page_th_schedule_delivery_plan';
			// write_log($ids);
			return $ids;
		}

		/**
		 * Function for set plugin action links.
		 *
		 * @param      string    $links    The action link.
		 *
		 * @return array
		 */
		public function plugin_action_links($links) {
			// write_log($links);
			$settings_link = '<a href="'.admin_url('edit.php?post_type=product&page=th_schedule_delivery_plan').'">'. esc_html__('Settings', 'schedule-delivery-for-woocommerce-free') .'</a>';
			array_unshift($links, $settings_link);
			// write_log($links);
			return $links;
		}

		/**
		 * Function for set plugins row meta.
		 *
		 * @param      string    $links    The action link.
		 * @param      string    $file    The plugin base name.
		 *
		 * @return array
		 */
		public function plugin_row_meta($links, $file) {
			if(THSDF_BASE_NAME == $file) {
				$doc_link = esc_url('https://www.themehigh.com/help-guides/schedule-delivery-for-woocommerce/');
				// write_log($doc_link);
				$support_link = esc_url('https://www.themehigh.com/help-guides/');
					
				$row_meta = array(
					'docs' => '<a href="'.$doc_link.'" target="_blank" aria-label="'.esc_attr__('View plugin documentation', 'schedule-delivery-for-woocommerce-free').'">'.esc_html__('Docs', 'schedule-delivery-for-woocommerce-free').'</a>',
					'support' => '<a href="'.$support_link.'" target="_blank" aria-label="'. esc_attr__('Visit premium customer support', 'schedule-delivery-for-woocommerce-free') .'">'. esc_html__('Premium support', 'schedule-delivery-for-woocommerce-free') .'</a>',
				);

				return array_merge($links, $row_meta);
			}
			return (array) $links;
		}

		/**
		 * Function for output settings.
		 */
		public function output_settings() {
			$tab  = isset($_GET['tab']) ? esc_attr($_GET['tab']) : 'general_settings';
			// THSDF_Utils::write_log($tab);
			if($tab === 'display_settings') {
				$display_settings = THSDF_Admin_Settings_Display::instance();
				$display_settings->render_page();
			}else{
				$general_settings = THSDF_Admin_Settings_General::instance();
				$general_settings->render_page();
			}
		}

		/**
		 * Function for calling the product page delivery setting class.
		 */
		public function product_page_delivery_settings() {
			$ptdelivery_product_settings = new THSDF_Product_Settings();
		}

		/**
		 * Function for calling the order page delivery setting class.
		 */
		public function order_page_delivery_settings() {
			$order_page = new THSDF_Admin_Order_Page_Settings();
		}		
	}
endif;