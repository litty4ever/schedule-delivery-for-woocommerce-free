<?php
/**
 * Define the internationalization functionality.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    schedule-delivery-for-woocommerce
 * @subpackage schedule-delivery-for-woocommerce/includes
 */
if(!defined('WPINC')) {	
	die; 
}

if(!class_exists('THSDF_i18n')) :
	/**
     * The translator(i18n) class.
     */
	// write_log('hjjj');
	class THSDF_i18n {
		const TEXT_DOMAIN = 'schedule-delivery-for-woocommerce-free';
		const ICL_CONTEXT = 'schedule-delivery-for-woocommerce-free';
		const ICL_NAME_PREFIX = "SDF";
		
		/**
		 * Load the plugin text domain for translation.
		 */
		public function load_plugin_textdomain() {
			$locale = apply_filters('plugin_locale', get_locale(), self::TEXT_DOMAIN);
			// write_log($locale);
			load_textdomain(self::TEXT_DOMAIN, WP_LANG_DIR.'/'.self::TEXT_DOMAIN.'/'.self::TEXT_DOMAIN.'-'.$locale.'.mo');
			load_plugin_textdomain(self::TEXT_DOMAIN, false, dirname(dirname(plugin_basename(__FILE__))) . '/languages/');
		}
		
		/**
		 * Function for get local code.
		 *
		 * @return string
		 */
		public static function get_locale_code() {
			$locale_code = '';
			$locale = get_locale();
			if(!empty($locale)) {
				$locale_arr = explode("_", $locale);
				if(!empty($locale_arr) && is_array($locale_arr)) {
					$locale_code = $locale_arr[0];
				}
			}	
			// write_log(empty($locale_code) ? 'en' : $locale_code);	
			return empty($locale_code) ? 'en' : $locale_code;
		}	

		/**
		 * Function of __t.
		 *
         * @param string $text the text string 
		 *
		 * @return string
		 */
		public static function __t($text) {
			if(!empty($text)) {	
				$otext = $text;						
				$text = __($text, self::TEXT_DOMAIN);	
				if($text === $otext) {
					$text = self::icl_t($text);
					if($text === $otext) {	
						$text = __($text, 'woocommerce');
					}
				}
			}
			return $text;
		}

		/**
		 * Function of t.
		 *
         * @param string $text the text string 
		 *
		 * @return string
		 */
		public static function t($text) {
			if(!empty($text)) {	
				$otext = $text;						
				$text = __($text, self::TEXT_DOMAIN);	
				if($text === $otext) {
					$text = self::icl_t($text);
					if($text === $otext) {	
						$text = __($text, 'woocommerce');
					}
				}
			}
			return $text;
		}
		
		/**
		 * Function of _et.
		 *
         * @param string $text the text string 
		 *
		 * @return string
		 */
		public static function _et($text) {
			if(!empty($text)) {	
				$otext = $text;						
				$text = __($text, self::TEXT_DOMAIN);	
				if($text === $otext) {
					$text = self::icl_t($text);
					if($text === $otext) {		
						$text = __($text, 'woocommerce');
					}
				}
			}
			echo $text;
		}

		/**
		 * Function of et.
		 *
         * @param string $text the text string 
		 *
		 * @return string
		 */
		public static function et($text) {
			if(!empty($text)) {	
				$otext = $text;						
				$text = __($text, self::TEXT_DOMAIN);	
				if($text === $otext) {
					$text = self::icl_t($text);
					if($text === $otext) {		
						$text = __($text, 'woocommerce');
					}
				}
			}
			echo $text;
		}
		
		/**
		 * Function of esc_attr__t.
		 *
         * @param string $text the text string 
		 *
		 * @return string
		 */
		public static function esc_attr__t($text) {
			if(!empty($text)) {	
				$otext = $text;						
				$text = esc_attr__($text, self::TEXT_DOMAIN);	
				if($text === $otext) {
					$text = self::icl_t($text);	
					if($text === $otext) {	
						$text = esc_attr__($text, 'woocommerce');
					}
				}
			}
			return $text;
		}
		
		/**
		 * Function of esc_html__t.
		 *
         * @param string $text the text string 
		 *
		 * @return string
		 */
		public static function esc_html__t($text) {
			if(!empty($text)) {	
				$otext = $text;						
				$text = esc_html__($text, self::TEXT_DOMAIN);	
				if($text === $otext) {
					$text = self::icl_t($text);	
					if($text === $otext) {	
						$text = esc_html__($text, 'woocommerce');
					}
				}
			}
			return $text;
		}
		
		/**
		 * WPML register string.
		 *
         * @param string $name The name string 
         * @param string $value The value string 
		 *
		 * @return void
		 */
		public static function wpml_register_string($name, $value) {
			$name = self::ICL_NAME_PREFIX." - ".$value;			
			if(function_exists('icl_register_string')) {
				icl_register_string(self::ICL_CONTEXT, $name, $value);
			}
		}
		
		/**
		 * WPML unregister string.
		 *
         * @param string $name The name string
		 *
		 * @return void
		 */
		public static function wpml_unregister_string($name) {
			if(function_exists('icl_unregister_string')) {
				icl_unregister_string(self::ICL_CONTEXT, $name);
			}
		}
		
		/**
		 * WPML icl_t.
		 *
         * @param string $value The value string
		 *
		 * @return string
		 */
		public static function icl_t($value) {
	        $name = self::ICL_NAME_PREFIX." - ".$value;
			
			if(function_exists('icl_t')) {
				$value = icl_t(self::ICL_CONTEXT, $name, $value);
			}
			return $value;
		}
	}
endif;