<?php
/**
 * The file that defines the core plugin class.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    schedule-delivery-for-woocommerce-free
 * @subpackage schedule-delivery-for-woocommerce-free/includes
 */

if(!defined('WPINC')) {	
	die; 
}
// write_log('sdcfv');
if(!class_exists('THSDF')) :

	/**
     * The plugin main class.
     */
	// write_log('cbb');
	class THSDF {

		/**
		 * The loader that's responsible for maintaining and registering all hooks that power
		 * the plugin.
		 *
		 * @access   protected
		 * @var      $loader    Maintains and registers all hooks for the plugin.
		 */
		protected $loader;

		/**
		 * The unique identifier of this plugin.
		 *
		 * @access   protected
		 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
		 */
		protected $plugin_name;

		/**
		 * The current version of the plugin.
		 *
		 * @access   protected
		 * @var      string    $version    The current version of the plugin.
		 */
		protected $version;

		/**
		 * Define the core functionality of the plugin.
		 *
		 * Set the plugin name and the plugin version that can be used throughout the plugin.
		 * Load the dependencies, define the locale, and set the hooks for the admin area and
		 * the public-facing side of the site.
		 */
		public function __construct() {
			if (defined('THSDF_VERSION')) {
				$this->version = THSDF_VERSION;
				// $this->write_log('sdf');
			} else {
				$this->version = '1.0.0';
			}
			$this->plugin_name = 'schedule-delivery-for-woocommerce-free';

			$this->load_dependencies();
			$this->set_locale();
			$this->define_admin_hooks();
			$this->define_public_hooks();
			add_action('init', array($this, 'init'));
			// $this->loader->add_filter('thsdf_custom_section_positions', 'THSDF_Utils', 'custom_step_hooks');

		}

		/**
		 * The init function. 
		 */
		public function init(){
			$this->define_constants();
		}

		/**
		 * The constants define function. 
		 */
		private function define_constants() {
			!defined('THSDF_ASSETS_URL_ADMIN') && define('THSDF_ASSETS_URL_ADMIN', THSDF_URL . 'admin/assets/');
			!defined('THSDF_ASSETS_URL_PUBLIC') && define('THSDF_ASSETS_URL_PUBLIC', THSDF_URL . 'public/assets/');
			!defined('THSDF_WOO_ASSETS_URL') && define('THSDF_WOO_ASSETS_URL', WC()->plugin_url() . '/assets/');
			// !defined('THSDF_TEMPLATE_PATH') && define('THSDF_TEMPLATE_URL', THWSD_PATH . 'public/templates/');
		}

		/**
		 * Load the required dependencies for this plugin.
		 *
		 * Include the following files that make up the plugin:
		 *
		 * - THSDF_Loader. Orchestrates the hooks of the plugin.
		 * - THSDF_i18n. Defines internationalization functionality.
		 * - THSDF_Admin. Defines all hooks for the admin area.
		 * - THSDF_Public. Defines all hooks for the public side of the site.
		 *
		 * Create an instance of the loader which will be used to register the hooks
		 * with WordPress.
		 *
		 * @access   private
		 */
		private function load_dependencies() {
			// write_log('ghf');
			if(!function_exists('is_plugin_active')) {
				include_once(ABSPATH . 'wp-admin/includes/plugin.php');
			}
			//require_once (plugin_dir_path(dirname(__FILE__)) . '/libraries/action-scheduler/action-scheduler.php');
			require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-thsdf-autoloader.php';

			/**
			 * The class responsible for orchestrating the actions and filters of the
			 * core plugin.
			 */
			require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-thsdf-loader.php';

			/**
			 * The class responsible for defining internationalization functionality
			 * of the plugin.
			 */
			require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-thsdf-i18n.php';

			/**
			 * The class responsible for defining all actions that occur in the admin area.
			 */
			require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thsdf-admin.php';

			/**
			 * The class responsible for defining all actions that occur in the public-facing
			 * side of the site.
			 */
			require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-thsdf-public.php';

			require_once plugin_dir_path(dirname(__FILE__)) . 'includes/utils/class-thsdf-utils.php';
			// //require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thwsd-admin.php';
			// require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thsdf-admin-utils.php';
			require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thsdf-admin-settings.php';
			require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thsdf-admin-settings-general.php';
			require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thsdf-admin-settings-display.php';
			// require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thsdf-admin-settings-license.php';
			require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thsdf-admin-product-page-settings.php';
			require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thsdf-admin-product-display-setting.php';
			require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-thsdf-admin-product-general-setting.php';

			// //require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-thwsd-public.php';
			// require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-thsdf-public-payment.php';
			//require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-thsdf-public-account.php';
			// $this->loader = new THSDF_Loader();
		}

		/**
		 * Define the locale for this plugin for internationalization.
		 *
		 * Uses the THWSD_i18n class in order to set the domain and to register the hook
		 * with WordPress.
		 *
		 * @access   private
		 */
		private function set_locale() {
			$plugin_i18n = new THSDF_i18n($this->get_plugin_name());
			// $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
			add_action('plugins_loaded',array($plugin_i18n, 'load_plugin_textdomain'));
		}

		/**
		 * Register all of the hooks related to the public-facing functionality
		 * of the plugin.
		 *
		 * @access   private
		 */
		private function define_admin_hooks() {
			$plugin_admin = new THSDF_Admin($this->version, $this->plugin_name);

			add_action('admin_enqueue_scripts', array($plugin_admin, 'enqueue_styles_and_scripts'));
			add_action('admin_menu', array($plugin_admin, 'admin_menu'));
			add_filter('woocommerce_screen_ids', array($plugin_admin, 'add_screen_id'));
			add_filter('plugin_action_links_'.THSDF_BASE_NAME, array($plugin_admin, 'plugin_action_links'));
			add_filter('plugin_row_meta', array($plugin_admin, 'plugin_row_meta'), 10, 2);

			// Product page settings
			add_action('init', array($plugin_admin, 'product_page_delivery_settings'));

			// // Order page settings
			add_action('init', array($plugin_admin, 'order_page_delivery_settings'));

			// // Auto completion daily check.
			// // if(!has_action('order_auto_completion_daily_check')) {
			// // 	add_action('order_auto_completion_daily_check', array($plugin_admin, 'ptdt_order_auto_completion_update_status'));
			// // }
		}

		/**
		 * Register all of the hooks related to the public-facing functionality
		 * of the plugin.
		 *
		 * @access   private
		 */
		private function define_public_hooks() {
			$plugin_public = new THSDF_Public($this->version, $this->plugin_name);
			add_action('wp_enqueue_scripts', array($plugin_public, 'enqueue_styles_and_scripts'));
		}

		/**
		 * Run the loader to execute all of the hooks with WordPress.
		 */
		public function run() {
			$this->loader->run();
		}

		/**
		 * The name of the plugin used to uniquely identify it within the context of
		 * WordPress and to define internationalization functionality.
		 *
		 * @return    string    The name of the plugin.
		 */
		public function get_plugin_name() {
			return $this->plugin_name;
		}

		/**
		 * The reference to the class that orchestrates the hooks with the plugin.
		 *
		 * @return    Loader Object    Orchestrates the hooks of the plugin.
		 */
		public function get_loader() {
			return $this->loader;
		}

		/**
		 * Retrieve the version number of the plugin.
		 *
		 * @return    string    The version number of the plugin.
		 */
		public function get_version() {
			return $this->version;
		}
		
	}

endif;





