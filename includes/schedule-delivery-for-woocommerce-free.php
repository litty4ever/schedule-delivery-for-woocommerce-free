<?php
/**
 * Plugin Name:     Schedule Delivery for Woocommerce free
 * Description:     Let your shoppers plan purchase deliveries for required quantity on a weekly or monthly basis.
 * Version:         1.0.0
 * Author:         	ThemeHigh
 * Author URI:		https://themehigh.com/ 
 *
 * Text Domain:     schedule-delivery-for-woocommerce-free
 * Domain Path:     /languages
 *
 * WC requires at least: 3.0.0
 * WC tested up to: 5.2.2
 */

if(!defined('WPINC')) {	
	die; 
}

if (!function_exists('is_woocommerce_active')){
	function is_woocommerce_active(){
	    $active_plugins = (array) get_option('active_plugins', array());
	    // write_log($active_plugins);
	    if(is_multisite()){
		   $active_plugins = array_merge($active_plugins, get_site_option('active_sitewide_plugins', array()));
	    }
	    if(in_array('woocommerce/woocommerce.php', $active_plugins) || array_key_exists('woocommerce/woocommerce.php', $active_plugins) || class_exists('WooCommerce')) {
	        return true;
	    } else {
	        return false;
	    }
	}
}

// $tt = is_woocommerce_active();
// write_log($tt);
if(is_woocommerce_active()) {
	define('THSDF_VERSION', '1.2.1');
	!defined('THSDF_SOFTWARE_TITLE') && define('THSDF_SOFTWARE_TITLE', 'Schedule Delivery for WooCommerce Free');
	!defined('THSDF_FILE') && define('THSDF_FILE', __FILE__);
	!defined('THSDF_PATH') && define('THSDF_PATH', plugin_dir_path(__FILE__));
	!defined('THSDF_URL') && define('THSDF_URL', plugins_url( '/', __FILE__));
	!defined('THSDF_BASE_NAME') && define('THSDF_BASE_NAME', plugin_basename( __FILE__));

	/**
	 * The core plugin class that is used to define internationalization,
	 * admin-specific hooks, and public-facing site hooks.
	 */
	require plugin_dir_path(__FILE__) . 'includes/class-thsdf.php';

	/**
	 * Begins execution of the plugin.
	 */
	function run_thsdf() {
		$plugin = new THSDF();
		// write_log($plugin);
	}
	run_thsdf();
}


// function write_log ( $log )  {
// 	if ( true === WP_DEBUG ) {
// 			if ( is_array( $log ) || is_object( $log ) ) {
// 				error_log( print_r( $log, true ) );
// 			} else {
// 				error_log( $log );
// 		}
// 	}
// }


