<?php
/**
 * The common utility functionalities for the plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    schedule-delivery-for-woocommerce-free
 * @subpackage schedule-delivery-for-woocommerce/includes-free/utils
 */
if(!defined('WPINC')) {	
	die; 
}

if(!class_exists('THSDF_Utils')) :

	/**
     * The Utils class.
     */
	class THSDF_Utils {
		const OPTION_KEY_DISPLAY_SETTINGS = '_thptdelivery_display_settings';
		const OPTION_KEY_DELIVERY_SETTINGS = '_thptdelivery_general_settings';
		const POST_KEY_PLAN_DELIVERY_CHECKBOX = '_plan_delivery_set';
		const POST_KEY_PLAN_DELIVERY_GENERAL = '_plan_delivery_global_set';
		const POST_KEY_SINGLE_SETTINGS = '_thptd_single_calendar_settings';
		const POST_KEY_SINGLE_DISPLAY_SETTINGS = '_thptd_single_display_settings';
		const ORDER_KEY_DELIVERY_DATE='_ptdt_delivery_date';
		const ORDER_KEY_USER_DELETE='_ptdt_user_deleted_orders';
		const POST_DELIVERY_DATE = '_thptd_delivery_date';
		const ORDER_DELETED_ITEM = '_order_deleted_items';
		
		/**
         * Function for get setting value.
         *
         * @param string $key The setting key
         * @param string $sections The section name
         * @param array $settings The setting datas
         *
         * @return string
         */
        public static function get_setting_value($key, $function_tag,$product_id='', $settings=false) {
            if(!$settings && !empty($function_tag)) {
            	$function_name = 'get_'.$function_tag;
                $settings = self::$function_name($product_id); 
            }
            if(is_array($settings) && isset($settings[$key])) {
                return $settings[$key];
            }
            return '';
        }

        /**
         * The general settings function.
         *
         * @return array
         */
        public static function get_general_settings($product_id) {
            $default_general_settings = self::default_general_settings();
            $settings = get_option(self::OPTION_KEY_DELIVERY_SETTINGS);
            // THSDF_Utils::write_log($settings);
            return empty($settings) ? $default_general_settings : $settings;
        }
        public static function get_product_general_settings($product_id) {
            $default_general_settings = self::default_general_settings();
            $save_calender_settings = get_post_meta($product_id, self::POST_KEY_SINGLE_SETTINGS, true);
            $save_general_settings = get_option(self::OPTION_KEY_DELIVERY_SETTINGS);
            if(!empty($save_calender_settings)) {
                $settings = $save_calender_settings;
            }else{
                if(!empty($save_general_settings)) {
                    $settings = $save_general_settings;
                }
            }
            return empty($settings) ? $default_general_settings : $settings;
        }

        public static function get_product_display_settings($product_id) {
            $default_display_settings = self::default_display_settings();
            $temp = array(
                'product_id'        => ''
                    );
            $default_display_pro_settings = array_merge($default_display_settings,$temp);
            $saved_display_settings = get_post_meta($product_id, THSDF_Utils::POST_KEY_SINGLE_DISPLAY_SETTINGS, true);
            $saved_settings = get_option(THSDF_Utils::OPTION_KEY_DISPLAY_SETTINGS);
            $genereal_setting = array();
            if(!empty($saved_settings)) {
                $product_data = array(
                    'product_id'        => '');
                $genereal_setting = array_merge($product_data, $saved_settings);
            }
            if(!empty($saved_display_settings)) {
                $settings = !empty($saved_display_settings) ? $saved_display_settings : $default_display_settings ;
            } else {
                $settings = !empty($genereal_setting) ? $genereal_setting : $default_display_settings ;
            }
            return $settings;

        }
        /**
         * Function for get default display settings.
         *
         * @return array
         */
		public static function get_display_settings($product_id) {		
			$default_display_settings = self::default_display_settings();
			$saved_settings = get_option(THSDF_Utils::OPTION_KEY_DISPLAY_SETTINGS);				
			$settings = !empty($saved_settings) ? $saved_settings : $default_display_settings ;
			return $settings;
		}



        public static function default_general_settings(){
            return array(
                'week_start' =>array(
                    '0'=>'sunday'
                ),
                'delivery_day' => array(
                    '0'=>'7'
                ),
                'start_date' => '',
                'end_date' => '',
                'holiday' => array(),
                'plan_display' => array(
                    '0'=>'4'
                )
                
            );
        }

        public static function default_display_settings(){
            return array(
                'primary_color'     => '#e5e5e6',
                'secondary_color'   => '#c4c4c4',
                'holiday_columns'   => '#f8f8f8',
                'tooltip_bg'        => '#ffffff',
                'general_color'     => '#6d6d6d',
                'input_value'       => '#43454b',
                'tooltip'           => '#444444',
            );
        }
		/**
         * The writelog function.
         *
         * @param string $log the log string
         *
         * @return string
         */
		public static function write_log ($log)  {
			if (true === WP_DEBUG) {
				if (is_array($log) || is_object($log)) {
					error_log(print_r($log, true));
				} else {
					error_log($log);
				}
			}
		}

        /**
         * Check the quick view plugin active.
         *
         * @return void
         */
        public static function is_quick_view_plugin_active() {
            $quick_view = false;
            if(self::is_flatsome_quick_view_enabled()) {
                $quick_view = 'flatsome';
            }else if(self::is_yith_quick_view_enabled()) {
                $quick_view = 'yith';
            }else if(self::is_astra_quick_view_enabled()) {
                $quick_view = 'astra';
            }
            return apply_filters('thwepo_is_quick_view_plugin_active', $quick_view);
        }
        
        /**
         * Check the theme yith quick view enabled.
         */
        public static function is_yith_quick_view_enabled() {
            return is_plugin_active('yith-woocommerce-quick-view/init.php');
        }
        
        /**
         * Check the theme flatsome quick view enabled.
         */
        public static function is_flatsome_quick_view_enabled() {
            return (get_option('template') === 'flatsome');
        }

        /**
         * Check the theme astra quick view enabled.
         */
        public static function is_astra_quick_view_enabled() {
            return is_plugin_active('astra-addon/astra-addon.php');
        }

        /**
         * Check the woocommerce version.
         *
         * @param string $version the woocommerce version
         *
         * @return string
         */
        public static function woo_version_check($version = '3.0') {
            if(function_exists('is_woocommerce_active') && is_woocommerce_active()) {
                global $woocommerce;
                if(version_compare($woocommerce->version, $version, ">=")) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Function for check the current actived theme.
         *
         * @return string
         */
        public static function check_current_theme() {
            $current_theme = wp_get_theme();
            $current_theme_name = isset($current_theme['Template']) ? $current_theme['Template'] : '';
            $wrapper_class = '';
            $theme_class_name = '';
            if($current_theme_name) {
                $wrapper_class = str_replace(' ', '-', strtolower($current_theme_name));
            }
            return $wrapper_class;
        }



	}
	
endif;