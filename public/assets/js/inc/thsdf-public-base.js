var thsdf_public_base = (function($, window, document) {
	'use strict';
	
	var DATE_FORMAT_1 = /^(19|20)\d{2}-(0?[1-9]|1[0-2])-(0?[1-9]|1\d|2\d|3[01])$/;
	var DATE_FORMAT_2 = /^(0?[1-9]|1[0-2])\/(0?[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
	var weekDays = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
	
	$.fn.getType = function(){
		try{
			return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); 
		}catch(err) {
			return 'E001';
		}
	}
	
	function remove_duplicates(arr){
	    var unique = arr.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        })   
        return unique;
	}
	
	function padZero(s, len, c){
		s = ""+s;
		var c = c || '0';
		while(s.length< len) s= c+ s;
		return s;
	}
	
	function isInt(value) {
	  	return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
	}
	
	function isEmpty(val){
		return (val === undefined || val == null || val.length <= 0) ? true : false;
	}
				
	function may_parse_date(dateStr){
		if(DATE_FORMAT_1.test(dateStr)){
			var date = new Date(dateStr);
			if(date){
				return date;
			}
		}
		return dateStr;
	}
	
	function prepare_date(dateStr, format, strict){
		var date = null;
		
		if(!isEmpty(dateStr)){
			try{
				date = $.datepicker.parseDate(format, dateStr);
				date.setHours(0,0,0,0);
			}catch(err) {
				if(!strict){
					var pattern = dateStr.split(" ");
					var years = null;
					var months = null;
					var days = null;
			
					if(pattern.length > 0){
						for(var i = 0; i < pattern.length; i++) { 
							var x = pattern[i];
							x = x.toLowerCase();
							
							if(x.indexOf("y") != -1){
								x = x.replace(/y/gi, "");
								years = parseInt(x);
							}else if(x.indexOf("m") != -1){
								x = x.replace(/m/gi, "");
								months = parseInt(x);
							}else if(x.indexOf("d") != -1){
								x = x.replace(/d/gi, "");
								days = parseInt(x);
							}
						}
					}
					
					if(!isEmpty(years) || !isEmpty(months) || !isEmpty(days)){
						date = new Date();
						date.setHours(0,0,0,0);
						
						if(years && years != 0){
							date.setFullYear(date.getYear() + years);
						}
						if(months && months != 0){
							date.setMonth(date.getMonth() + months);
						}
						if(days && days != 0){
							date.setDate(date.getDate() + days);
						}
					}
				}
			}
		}
		return date;
	}
	
	function compare_dates(field, cvalue){
		var result = null;
		var value = field.val();
		var format = field.data("date-format");
		
		if(isEmpty(value) || isEmpty(cvalue)){
			return null;
		}
		
		var d1 = prepare_date(value, format, true);
		var d2 = prepare_date(cvalue, format, false);
		
		if(d1 && d2){
			try{
				if(d1 > d2){
					result = 1; 
				}else if(d1 < d2){
					result = -1; 
				}else if(d1.getTime() === d2.getTime()){
					result = 0; 
				}
			}catch(err) {
				result = null;
			}
		}
		return result;
	}
	
	function isSameDate(date1, date2){
		var day1 = date1.getDate();
		var month1 = date1.getMonth() + 1;
		var year1 = date1.getFullYear();
		
		var day2 = date2.getDate();
		var month2 = date2.getMonth() + 1;
		var year2 = date2.getFullYear();
		
		var matchYear = isInt(day1) && isInt(day2) && (day1 == day2) ? true : false;
		var matchMonth = isInt(month1) && isInt(month2) && (month1 == month2) ? true : false;
		var matchDay = isInt(year1) && isInt(year2) && (year1 == year2) ? true : false;
		
		return matchYear && matchMonth && matchDay;
	}
	
	function is_date_eq(field, cvalue){
		var result = compare_dates(field, cvalue);
		return (result != null && result === 0) ? true : false;
	}
	
	/*function is_date_ne(field, cvalue){
		var result = compare_dates(field, cvalue);
		return result ? true : false;
	}*/
	
	function is_date_gt(field, cvalue){
		var result = compare_dates(field, cvalue);
		return (result != null && result === 1) ? true : false;
	}
	
	function is_date_lt(field, cvalue){
		var result = compare_dates(field, cvalue);
		return (result != null && result === -1) ? true : false;
	}
	
	function is_day_eq(field, cvalue){
		var result = false;
		
		if(!isEmpty(cvalue)){
			var value = field.val();
			var format = field.data("date-format");
			var date = prepare_date(value, format, true);
			
			if(date){
				var day = date.getDay();
				//var daysArr = cvalue.split(",");
				if(isInt(cvalue)){
					cvalue = parseInt(cvalue);
					result = (day != null && day === cvalue) ? true : false;
				}else {
					cvalue = cvalue.toLowerCase();
					if($.inArray(cvalue, weekDays) >= 0){
						var daystring = weekDays[day];
						result = (daystring != null && daystring === cvalue) ? true : false;
					}
				}
			}
		}
		return result;
	}
	
	function setup_enhanced_select(form, class_selector){
		form.find('select.'+class_selector).select2({
			minimumResultsForSearch: 10,
			allowClear : true,
			placeholder: $(this).data('placeholder')
		}).addClass('enhanced');
	}
	
	
	
	function no_sundays(date) {
		var day = date.getDay();
		return [day != 0, ''];
	}
	function no_saturdays(date) {
		var day = date.getDay();
		return [day != 6, ''];
	}
	function no_weekends(date) {
		return $.datepicker.noWeekends(date);
	}
	function no_christmas(date) {
		var day = date.getDate();
		var month = date.getMonth() + 1;
		return [!(day === 25 && month === 12), ''];
	}
	function no_new_years_day(date) {
		var day = date.getDate();
		var month = date.getMonth() + 1;
		return [!(day === 1 && month === 1), ''];
	}
	function no_holidays(date) {
		var datestring = $.datepicker.formatDate('yy-mm-dd', date);
    	return [ holidays.indexOf(datestring) == -1, '' ];
	}
	
	function no_weekends_or_holidays(date) {
		var noWeekend = $.datepicker.noWeekends(date);
		if (noWeekend[0]) {
			return no_holidays(date);
		} else {
			return noWeekend;
		}
	}
	
	function no_specific_days(date, disableDays) {
		var day = date.getDay();
		var daystring = weekDays[day];
    	return [ disableDays.indexOf(daystring) == -1, '' ];
	}
	
	function no_specific_dates(date, datestring) {
		var day = date.getDate();
		var month = date.getMonth() + 1;
		var year = date.getFullYear();
		
		var dateArr = datestring.split("-");
		if(dateArr.length == 3){
			var matchYear = isInt(dateArr[0]) ? dateArr[0] == year : true;
			var matchMonth = isInt(dateArr[1]) ? dateArr[1] == month : true;
			var matchDay = isInt(dateArr[2]) ? dateArr[2] == day : true;
			
			if(isInt(dateArr[0]) || isInt(dateArr[1]) || isInt(dateArr[2])){
				return [!(matchYear && matchMonth && matchDay), ''];
			}else{
				return [true, ''];
			}
		}else{
			var _now = new Date();
			// if(isSameDate(date, _now)){
			// 	var _hour = _now.getHours();
			// 	var _min = _now.getMinutes();
				
			// 	var op = "eq";
			// 	if(datestring.indexOf("+") != -1){
			// 		op = "gt";
			// 		datestring = datestring.replace("+", "");
			// 	}else if(datestring.indexOf("-") != -1){
			// 		op = "lt";
			// 		datestring = datestring.replace("-", "");
			// 	}
				
			// 	var _minutes = calculate_minutes_from_hr_min(_hour, _min);
			// 	var minutes = get_minutes_from_time_24hr(datestring);
				
			// 	if(isInt(minutes) && isInt(_minutes)){
			// 		if((op === "eq" && _minutes == minutes) || (op === "gt" && _minutes > minutes) || (op === "lt" && _minutes < minutes)){
			// 			return [false, ''];
			// 		}
			// 	}
			// }
		}
		return [true, ''];
	}
	
	function disable_dates(date){
		var disabledDays = $(this).data("disabled-days");
		if(disabledDays && disabledDays.length > 0){
			var daysArr = disabledDays.split(",");
			var disabledDay = no_specific_days(date, daysArr);
			
			if(!disabledDay[0]) {
				return disabledDay;
			}
			
			/*if(daysArr.length > 0){
				for (i = 0; i < daysArr.length; i++) { 
					var dayIndex = weekDays.indexOf(daysArr[i].trim());
					
					var disabled = noSpecificDays(date, dayIndex);
					if(!disabled[0]) {
						return disabled;
					}
				}
			}*/
		}
		
		var disabledDates = $(this).data("disabled-dates");
		if(disabledDates && disabledDates.length > 0){
			var datesArr = disabledDates.split(",");
			/*var disabledDate = noSpecificDates(date, datesArr);
			
			if(!disabledDate[0]) {
				return disabledDate;
			}*/
			if(datesArr.length > 0){
				for (var i = 0; i < datesArr.length; i++) { 
					var disabledDate = no_specific_dates(date, datesArr[i].trim());
					//alert(datesArr[i].trim()+":::"+disabledDate[0]);
					if(!disabledDate[0]) {
						return disabledDate;
					}
				}
			}
		}
		
		return [true, ''];
	}
	
	function setup_date_picker(form, class_selector, data){
		form.find('.'+class_selector).each(function(){
			var dateFormat = $(this).data("date-format");		
			var defaultDate = $(this).data("default-date");
			var maxDate = $(this).data("max-date");
			var minDate = $(this).data("min-date");
			var yearRange = $(this).data("year-range");
			var numberOfMonths = $(this).data("number-months");
			
			maxDate = may_parse_date(maxDate);
			minDate = may_parse_date(minDate);
							
			dateFormat = dateFormat == '' ? 'dd/mm/yy' : dateFormat;
			defaultDate = defaultDate == '' ? null : defaultDate;
			maxDate = maxDate == '' ? null : maxDate;
			minDate = minDate == '' ? null : minDate;
			yearRange = yearRange == '' ? '-100:+1' : yearRange;
			numberOfMonths = numberOfMonths > 0 ? numberOfMonths : 1;
			
			var value = $(this).val();
			if(value.trim()){
				defaultDate = value;
			}
			
			//minDate = new Date().getHours() >= 2 ? 1 : 0;
			
			$(this).datepicker({
				defaultDate: defaultDate,
				maxDate: maxDate,
				minDate: minDate,
				yearRange: yearRange,
				numberOfMonths: numberOfMonths,
				showButtonPanel: true,
				changeMonth: true,
				changeYear: true			
			});
			$(this).datepicker("option", $.datepicker.regional[data.language]);
			$(this).datepicker("option", "dateFormat", dateFormat);
			$(this).datepicker("option", "beforeShowDay", disable_dates);
			$(this).datepicker("setDate", defaultDate);

			if(data.readonly_date_field){
				$(this).prop('readonly', true);
			}
		});
	}
	/******************************************
	***** DATE PICKER FUNCTIONS - END *********
	******************************************/
	
    /******************************************
	***** TIME PICKER FUNCTIONS - START *******
	******************************************/
	// function split_hour_min(hourMinStr){
	// 	var hours = 0;
	// 	var minutes = 0;
		
	// 	if(hourMinStr && (typeof hourMinStr === 'string' || hourMinStr instanceof String)){
	// 		var _hourMin = hourMinStr.split(" ");
			
	// 		if(_hourMin.length > 0){
	// 			for(var i = 0; i < _hourMin.length; i++) { 
	// 				var x = _hourMin[i];
	// 				x = x.toLowerCase();
					
	// 				if(x.indexOf("h") != -1){
	// 					x = x.replace(/h/gi, "");
	// 					hours = parseInt(x);
	// 				}else if(x.indexOf("m") != -1){
	// 					x = x.replace(/m/gi, "");
	// 					minutes = parseInt(x);
	// 				}
	// 			}
	// 		}
			
	// 		hours = hours ? hours : 0;
	// 		minutes = minutes ? minutes : 0;
			
	// 		if(minutes >= 60){
	// 			hours = hours + 1;
	// 			minutes = 0;
	// 		}
	// 	}
		
	// 	return [hours, minutes];
	// }
	
	// function get_start_hr_min(startTime){
	// 	var timeInfo = {};
	// 	if(startTime){
	// 		var startTimeArr = split_hour_min(startTime);
	// 		if(startTimeArr.length > 1){
	// 			var currTime = new Date();
	// 			var currHour = currTime.getHours();
	// 			var currMin  = currTime.getMinutes();
				
	// 			var _startHour = startTimeArr[0];
	// 			var startDays = parseInt(_startHour/24);
	// 			var startDate = new Date();
	// 			startDate.addDays(startDays).setHours(0,0,0,0);
	// 			var startHour = _startHour%24;
	// 			var startMin  = startTimeArr[1];
				
	// 			startHour = currHour+startHour;
	// 			startMin  = currMin+startMin;
	// 			if(startMin >= 60){
	// 				startHour++;
	// 				startMin = startMin-60;
	// 			}else if(startMin < 0){
	// 				startHour--;
	// 				startMin = 60+startMin;
	// 			}
				
	// 			timeInfo['startDate'] = startDate;
	// 			timeInfo['startDays'] = startDays;
	// 			timeInfo['startHour'] = startHour;
	// 			timeInfo['startMin'] = startMin;
	// 			timeInfo['hour'] = startTimeArr[0];
	// 			timeInfo['min'] = startTimeArr[1];
	// 		}
	// 	}
	// 	return timeInfo;		
	// }
	
	// function get_time_suffix(time){
	// 	time = time.toLowerCase();
	// 	var suffix = "";
	// 	if(time.indexOf("am") != -1){
	// 		suffix = "am";
	// 	}else if(time.indexOf("pm") != -1){
	// 		suffix = "pm";
	// 	}
	// 	return suffix;
	// }
		
	// function split_time_string(time, ampm){
	// 	time = time.replace(/pm/gi, "");
	// 	time = time.replace(/am/gi, "");
	// 	var timeArr = time.split(":");
		
	// 	var hours = parseInt(timeArr[0]);
	// 	var minutes = parseInt(timeArr[1]);
		
	// 	if(ampm == "pm" && hours < 12){
	// 		hours = hours + 12;
	// 	}else if(ampm == "am" && hours == 12){
	// 		hours = hours - 12;
	// 	}
		
	// 	return [hours, minutes];
	// }
	
	// function split_time_string_12hr(time){
	// 	var ampm = get_time_suffix(time);
	// 	return split_time_string(time, ampm);
	// }
	
	// function get_disabled_time_ranges(minTime, maxTime, startTime){
	// 	var minHour = minTime[0];
	// 	var minMin = minTime[1];
		
	// 	var maxHour = maxTime[0];
	// 	var maxMin = maxTime[1];
		
	// 	var currTime = new Date();
	// 	var currHour = currTime.getHours();
	// 	var currMin  = currTime.getMinutes();
	// 	currTime.setSeconds(0, 0);
		
	// 	var startHour = startTime["startHour"];
	// 	var startMin = startTime["startMin"];
	// 	var startDate = new Date();
	// 	startDate.setHours(startHour, startMin, 0, 0);
				
	// 	minHour = padZero(minHour, 2);
	// 	minMin = padZero(minMin, 2);
		
	// 	startHour = padZero(startHour, 2);
	// 	startMin = padZero(startMin, 2);
		
	// 	var disMinRange = minHour+":"+minMin;
	// 	var disMaxRange = startHour+":"+startMin;
		
	// 	var disRange = [[disMinRange, disMaxRange]];
	// 	return disRange;
	// }
	
	// function disable_all_time_slots(tp, minTime, maxTime){
	// 	var suffixMaxTime = get_time_suffix(maxTime);
	// 	var maxTimeArr = split_time_string(maxTime, suffixMaxTime);
	// 	var maxHour = maxTimeArr[0];
	// 	var maxMin  = maxTimeArr[1];
		
	// 	maxHour = padZero(maxHour, 2);
	// 	maxMin = padZero(parseInt(maxMin)+1, 2);
							
	// 	var newMaxTime = maxHour+':'+maxMin;
	// 	tp.timepicker('option', 'disableTimeRanges', [[minTime, newMaxTime]]);
	// 	//TODO correct newMaxTime for border cases (24:00)
	// }
	
	// function adjust_time_slots_based_on_date_selected(dp, tp){
	// 	var dpDate = null;
		
	// 	if(dp){
	// 		var df = dp.data("date-format");
	// 		var sd = dp.val();
	// 		dpDate = prepare_date(sd, df, true);
	// 	}
	   	
	// 	var minTime = tp.data("min-time");
	//    	var maxTime = tp.data("max-time");
	// 	var startTime = tp.data("start-time");
		
	// 	var startTimeArr = get_start_hr_min(startTime);
	// 	if(startTimeArr){
	// 		var startDate = startTimeArr["startDate"];
			
	// 		if(dp != null && dpDate < startDate){
	// 			disable_all_time_slots(tp, minTime, maxTime);
	// 		}else if(dp != null && dpDate > startDate){
	// 			tp.timepicker('option', 'disableTimeRanges', []);
	// 		}else{
	// 			// If dates are equal check for current time
	// 			// If current time is gt maxTime then clear time slots
	// 			// If current time is lt minTime the set original minTime as minTime
	// 			// If current time is within in the allowed range then set minTime as next available slot.
	// 			var minTimeArr = split_time_string_12hr(minTime);
	// 			var minHour = minTimeArr[0];
	// 			var minMin = minTimeArr[1];
				
	// 			var maxTimeArr = split_time_string_12hr(maxTime);
	// 			var maxHour = maxTimeArr[0];
	// 			var maxMin = maxTimeArr[1];
				
	// 			var startHour = startTimeArr["startHour"];
	// 			var startMin = startTimeArr["startMin"];
						
	// 			if(startHour > maxHour || (startHour == maxHour && startMin > maxMin)){
	// 				disable_all_time_slots(tp, minTime, maxTime);					
	// 			}else if(startHour < minHour || (startHour == minHour && startMin < minMin)){
	// 				tp.timepicker('option', 'disableTimeRanges', []);
	// 			}else{
	// 				var disabledTimeRanges = get_disabled_time_ranges(minTimeArr, maxTimeArr, startTimeArr);
	// 				tp.timepicker('option', 'disableTimeRanges', disabledTimeRanges);
	// 			}				
	// 		}
	// 	}
	// }
	
	// function setup_time_picker(form, class_selector, data){
	// 	form.find('.'+class_selector).each(function(){
	// 		var minTime = $(this).data("min-time");		
	// 		var maxTime = $(this).data("max-time");
	// 		var step    = $(this).data("step");
	// 		var format  = $(this).data("format");
	// 		var startTime = $(this).data("start-time");
	// 		var linkedDate = $(this).data("linked-date");
							
	// 		minTime = minTime ? minTime : '12:00am';
	// 		maxTime = maxTime ? maxTime : '11:30pm';
	// 		step 	= step ? step : '30';
	// 		format 	= format ? format : 'h:i A';
			
	// 		var args = {
	// 			'minTime': minTime,
	// 			'maxTime': maxTime,
	// 			'step': step,
	// 			'timeFormat': format,
	// 			'forceRoundTime': true,
	// 			//'showDuration':true,
	// 			'disableTextInput' : true,
	// 			'lang': data.lang
	// 		}		
	// 		$(this).timepicker(args);
	// 		//$(this).timepicker('option', 'minTime', tpMinTime(format, step, minTime, maxTime, startTime));
			
	// 		if(linkedDate){
	// 			var dp = $("#"+linkedDate);
	// 			if( dp.length ) {
	// 				adjust_time_slots_based_on_date_selected(dp, $(this));
	// 			}
	// 		}else{
	// 			adjust_time_slots_based_on_date_selected(null, $(this));	
	// 		}
	// 	});
	// }
    /******************************************
	***** TIME PICKER FUNCTIONS - END *********
	******************************************/
	
	/********************************************
	***** CHARACTER COUNT FUNCTIONS - START *****
	********************************************/
	function display_char_count(elm, isCount){
		var fid = elm.prop('id');
        var len = elm.val().length;
		var displayElm = $('#'+fid+"-char-count");
		
		if(isCount){
			displayElm.text('('+len+' characters)');
		}else{
			var maxLen = elm.prop('maxlength');
			var left = maxLen-len;
			displayElm.text('('+left+' characters left)');
			if(rem < 0){
				displayElm.css('color', 'red');
			}
		}
	}
    /******************************************
	***** CHARACTER COUNT FUNCTIONS - END *****
	******************************************/
	
	function set_field_value_by_elm(elm, type, value){
		switch(type){
			case 'radio':
				elm.val([value]);
				break;
			case 'checkbox':
				if(elm.data('multiple') == 1){
					value = value ? value : [];
					elm.val(value);
				}else{
					elm.val([value]);
				}
				break;
			case 'select':
				if(elm.prop('multiple')){
					elm.val(value);
				}else{
					elm.val([value]);
				}
				break;
			default:
				elm.val(value);
				break;
		}
	}
	
	function get_field_value(type, elm, name){
		var value = '';
		switch(type){
			case 'radio':
				value = $("input[type=radio][name="+name+"]:checked").val();
				break;
			case 'checkbox':
				if(elm.data('multiple') == 1){
					var valueArr = [];
					$("input[type=checkbox][name='"+name+"[]']:checked").each(function(){
					   valueArr.push($(this).val());
					});
					value = valueArr;//.toString();
				}else{
					value = $("input[type=checkbox][name="+name+"]:checked").val();
				}
				break;
			case 'select':
				value = elm.val();
				break;
			case 'multiselect':
				value = elm.val();
				break;
			default:
				value = elm.val();
				break;
		}
		return value;
	}
	
	return {
		setup_enhanced_select : setup_enhanced_select,
		setup_date_picker : setup_date_picker,
		display_char_count : display_char_count,
		remove_duplicates : remove_duplicates,
		set_field_value_by_elm : set_field_value_by_elm,
		get_field_value : get_field_value,
		is_date_eq : is_date_eq,
		is_date_gt : is_date_gt,
		is_date_lt : is_date_lt,
		is_day_eq : is_day_eq,
	};
}(window.jQuery, window, document));
