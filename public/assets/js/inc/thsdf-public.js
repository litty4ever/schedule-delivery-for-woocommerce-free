(function( $ ) {
	'use strict';
	add_special_class();
	var deferentMonth = 0;
    var date_cal = $(document).find('#ptdelivery-date');  
    var pr_cal = $(document).find('#ptdelivery-calendar');
    
	// Cart Page.
	display_list_of_product_quantity();

	check_variation_changes();
    
	const months = [
   		thsdf_public_var.january,
        thsdf_public_var.february,
        thsdf_public_var.march,
        thsdf_public_var.april,
        thsdf_public_var.may,
        thsdf_public_var.june,
        thsdf_public_var.july,
        thsdf_public_var.august,
        thsdf_public_var.september,
        thsdf_public_var.october,
        thsdf_public_var.november,
        thsdf_public_var.december
    ];
   
    function thsdf_functions(){
		create_calendar();
		ptdt_calendar_tooltips();
		display_setting();
		calculate_quantity();

		$(document).on('click', '#thsdf_prev', function(){
			thsdf_prev();
		});

		$(document).on('click', '#thsdf_next', function(){
			thsdf_next();
		});

	}

 
    "flatsome" == thsdf_public_var.is_quick_view ? $(document).on("mfpOpen", function() {
        thsdf_functions();
        $('.ptdelivery-calendar').addClass('thsdf_quick_view');
    }) : ("yith" == thsdf_public_var.is_quick_view ? $(document).on("qv_loader_stop", function() {
        thsdf_functions()
    }) : "astra" == thsdf_public_var.is_quick_view && $(document).on("ast_quick_view_loader_stop", function() {
        thsdf_functions()
    })) 
	thsdf_functions();
	
	// TO DO
	calendar_popup(); // Cart Page.

	


	function thsdf_prev() {
	  	deferentMonth--;
	  	create_calendar();
	  	display_setting();
	  	ptdt_calendar_tooltips();
		
	}

	function thsdf_next() {
	  	deferentMonth++;
	  	create_calendar();
	  	display_setting();
	  	ptdt_calendar_tooltips();
		
	}

	function create_calendar() {
		var calendar_next = $(".ptdelivery-next");
		var calendar_prev = $(".ptdelivery-prev");
	  	var date = new Date();
		var d = new Date(date.getFullYear(), date.getMonth(), 1);
	  	var n = d.getDate();
	  	d.setMonth(d.getMonth() + deferentMonth);
	  	var year = d.getFullYear(),
	    month = d.getMonth(),
	    dayCount = (new Date(year, month + 1, 0)).getDate(),
	    dayStart = (new Date(year, month, 1)).getDay(); 
		if (dayStart === 0){
	        dayStart = 7;
	    }

	   var today = null;
	   
	    if (deferentMonth === 0) {
	   		today = d.getDate();
	    }


		// Input Start date.
		var start_date = jQuery('#ptdt_start_date').val();

		// Input Start month from scheduling start date.
		var schedule_start_date = jQuery('#ptdt_schedule_start_date').val();
		var s_date    		= new Date(schedule_start_date);
		var s_year      	= s_date.getFullYear();
		var s_month_1   		= s_date.getMonth()+1;
		var st_month   		= s_date.getMonth();
		var s_day     		= s_date.getDate();
		var s_position_value= s_date.getDay();
		var s_month = ("0" + (s_month_1)).slice(-2);
		var new_startDate = s_year + '-' + s_month;

		var s_dayCount = (new Date(s_year, s_month, 0)).getDate();
		var s_dayStart = (new Date(s_year, s_month-1, 1)).getDay();

		// Input End date.
		var end_date = jQuery('#ptdt_end_date').val();

		var date    		= new Date(end_date);
		var e_year      	= date.getFullYear();
		var e_month_1   		= date.getMonth()+1;
		var e_day     		= date.getDate();
		var e_position_value= date.getDay();
		var e_month = ("0" + (e_month_1)).slice(-2);
		var new_endDate = e_year + '-' + e_month;
	    var cur_month = month ;
	    var month = ("0" + (month+1)).slice(-2);
	    var current_date = year + '-' + (month); 

	    if((start_date != '') && (end_date != '')){
	    	if((new_startDate <= current_date) && (current_date <= new_endDate)){
			    printDate(d);
			    generate_calendar(dayCount, today, dayStart, year, cur_month, start_date, end_date);
			} else if(new_startDate > current_date){
				printDate(s_date);
				generate_calendar(s_dayCount, today, s_dayStart, s_year, st_month, start_date, end_date);
			}
			
			if(current_date == new_endDate){
				calendar_next.css("display", "none");
			} else if(current_date > new_endDate){
				calendar_next.css("display", "none");
			} else if(new_startDate  == new_endDate){
				calendar_next.css("display", "none");
			} else{
				calendar_next.css("display", "block");
			}

			if(current_date == new_startDate){
				calendar_prev.css("display", "none");
			} else if(new_startDate > current_date){
				calendar_prev.css("display", "none");
			} else{
				calendar_prev.css("display", "block");
			}
		}
	}

	function generate_calendar(count, today, start, year, month, start_date, end_date) {
		var input_text = $(".quantity .input-text.qty.text");
	    if ($("#ptdelivery-calendar").length > 0) {
	        input_text.attr("readonly", true);
    	    input_text.addClass( "ptdt-disabled-no-inc" );
	    }
	    if($('.thsdf-flatsome-calendar-wrapper-public #ptdelivery-calendar').length>0){
	    	$('.quantity.buttons_added input.plus.button.is-form').css('display','none');
	    	$('.quantity.buttons_added input.minus.button.is-form').css('display','none');
	    }

	    var delivery_date_info = calendar_submited_data();

		// Check to disable add to cart button.
		var form = $("form.cart");
		var variable_product = is_variable_product(form);
		var variation_id = 0;
		if ($("#ptdelivery-calendar").length > 0) {
		    if(typeof delivery_date_info == 'undefined' || delivery_date_info == "" || delivery_date_info == null){
				jQuery( ".cart button.single_add_to_cart_button.button" ).addClass( "disabled" );
				if(variable_product == true){
					jQuery( "button.single_add_to_cart_button" ).addClass( "variable-pdt-disabled" );
				}
			} else {
				jQuery( ".cart button.single_add_to_cart_button.button" ).removeClass( "disabled" );
				if(variable_product == true){
					jQuery( "button.single_add_to_cart_button" ).removeClass( "variable-pdt-disabled" );
				}
			}
		}

		// Current date.
		var ptdt_current_date = jQuery('#ptdt_current_date').val();
		if(ptdt_current_date > start_date){
			start_date = ptdt_current_date;
		}

		// Calendar sumbmited data.
		var calendar_input_field_data = calendar_submited_data();

		// Set Week start.
		var week_start = jQuery('#ptdt_week_start_days').val();
	    var day_num = jQuery('#ptdt_week_start_days').data('daynum');

	    // Validate json.
	    var obj_daysOfWeek = [];
		var disable_days = [];
		var disable_holidays = [];
	    if(week_start != undefined) {
			var obj_daysOfWeek = jQuery.parseJSON(week_start);
		}
		var $count_days = obj_daysOfWeek ? obj_daysOfWeek.length : '';

		// Disable week days.
		var disable_days = jQuery('#ptdt_week_disable_days').val();
		// Validate json.
		var obj_disable_days = '';
		if(disable_days != undefined) {
			var obj_disable_days = jQuery.parseJSON(disable_days);
		}

		// Disable holidays.
		var disable_holidays = jQuery('#ptdt_week_holidays').val();
		// Validate json.
		var obj_disable_holidays = '';
		if(disable_holidays != undefined) {
			var obj_disable_holidays = jQuery.parseJSON(disable_holidays);
		}

		var product_id = jQuery('#ptdt_product_id').val();

		var html = '';

		html += '<tr>';
	  	$.each( obj_daysOfWeek, function( index, value ) {
	  		var new_week_days = value.slice(0,3);
	  		var calendar_wrapper_public = $( ".ptdt-calendar-wrapper-public" );
	  		if(($( ".entry-summary" ).width()<=417) && ($( ".entry-summary" ).width() != null)){
	  	        value = new_week_days; 
	  	        calendar_wrapper_public.addClass( "thsdf-calendar-day-min-screen" );
	  	    }
	  		
	  		if(($(window).width() <= 960) && ($(window).width() != null)) {
	        	value = new_week_days;
		    } else {
		        value = value;
		    }
		    var flatsome_wrap_width = $('.thsdf-flatsome-calendar-wrapper-public').width();
		    if((flatsome_wrap_width<=360) && (flatsome_wrap_width != null)){
	        	value = new_week_days;
	        	calendar_wrapper_public.addClass( "thsdf-calendar-day-min-screen" );
		    } else {
		        value = value;
		    }
		      
	  		html += '<th class="ptdelivery-calendar-header"><div class="calendar-week-day ptdt_primary ptdt_secondary ptdt_general_color">'+ value +'</div></th>';
		});

	   	html += '</tr><tr>';
	   	var newstart = start-day_num;

	    if ((newstart > 0) && (newstart != $count_days)){
		   html += '<td colspan="'+ newstart +'" class="ptdelivery-calendar-day ptdelivery-calendar-other">&nbsp;</td>'; 
	    } else if (newstart < 0){ 
	    	var newstart = $count_days-day_num+start;
	    	html += '<td colspan="'+ newstart +'" class="ptdelivery-calendar-day ptdelivery-calendar-other">&nbsp;</td>';
	    } else{

	    }
	  	for (var i = 1; i <= count; i++) {

	  		if (newstart == 7) {

		        newstart = 0;
		        html += '</tr><tr>';
			 }

			var n = i;
			
			var currentmonth = ("0" + (month + 1)).slice(-2);
			var currentDayRel = ("0" + (n)).slice(-2);
			var date = year+'-'+currentmonth+'-'+currentDayRel;

			// Get week day not available.
			var not_avail = date_available(date,obj_disable_days);
			var class_day = '';
			var disabled = '';
			if(not_avail == date) {
				class_day = 'ptdf_disable_day ptdt_tooltip_view ptdt_holiday';
				disabled = 'disabled';
			}
			
			// Disable before start day.
			if(date < start_date){
				class_day = 'ptdf_befor_day';
				disabled = 'disabled';
			}

			//Disable after end day.
			if(date > end_date){
				class_day = 'ptdf_after_day';
				disabled = 'disabled';
			}

			// Get holidays.
			var holidays = date_available(date,obj_disable_holidays);
			if(holidays == date){
				class_day = 'ptdf_holidays ptdt_tooltip_view ptdt_holiday';
				disabled = 'disabled';
			}

			// Get submited date.
			var input_value = '';
			if(calendar_input_field_data != null && calendar_input_field_data != ''){				
				$.each( calendar_input_field_data, function( key, value ){ 				
					var product_date = key;
					var product_qty_no = value;
					if(product_date == date){
						input_value = product_qty_no;
					}
				});
			}
		    var cl = 'ptdelivery-calendar-day';
		    if (i === today) {
		    	cl += ' today';
		    }
		    html += '<td class="ptdt_date_td ' + class_day + ' ' +cl + '" rel="' + date +'">';
		    html +=	'<div class="ptdelivary-single-date">';		
		    html +=	'<span class="ptdelivary-date-day ptdt_general_color">' + i +'</span>';
		    html +=	'<input type="number" name="ptdelivery_product_number" class="ptdelivery-number-input ptdt_input_color" data-date="' + date + '" '+ disabled + ' value="' + input_value + '" min="1" >';
		    html +=	'<div id="inc-button" class="spinner-button"><span class="dashicons dashicons-arrow-down"></span></div>';
			html +=	'<div id="dec-button" class="spinner-button"><span class="dashicons dashicons-arrow-up"></span></div>';
		    html +=	'</div>';
		    html +=	'</td>';
		    
			newstart++;

		}

	  	if (newstart != 7) { 
		    var remainingDays = 7 - newstart;
		    html += '<td colspan="' + remainingDays + '" class="ptdt_date_td ptdelivery-calendar-day">&nbsp;</td>'; 
		}
		html += '</tr>';
		$(document).find('#ptdelivery-calendar').html(html);
	}

	function printDate(d) {
		var new_month_name = months[d.getMonth()];
		if(new_month_name != undefined){
			var month_name = new_month_name.replace(/[^a-zA-Z 0-9]+/g, "");
			var month_name_sub  = month_name .slice(0, 3);
			var month = capitalize(month_name_sub); 
			if(date_cal){
				$(document).find('#ptdelivery-date').html(month + " " + d.getFullYear());
			}
		}
	}

	function date_available(value,arr) {
		var status = 'Not exist';
		if(arr){
			for(var i=0; i<arr.length; i++){
			    var name = arr[i];
			    if(name == value){
			    	status = value;
			    	break;
			    }
			}
		}

		return status;
	}

	function capitalize (str) {
		return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
	};

	function ptdt_calendar_tooltips(){
		$(".ptdf_holidays, .ptdf_disable_day").hover(function() {
	        $(this).css('cursor','pointer').attr('title', thsdf_public_var.no_delivery);
	    }, function() {
	        //$(this).css('cursor','auto');
	    });

		var Delay = 100, ToolTipTimer
		$('.ptdt_tooltip_view').hover(function(e){
			var tooltip_bg = jQuery('#ptdt_tooltip_bg_code').val();
			var tooltip = jQuery('#ptdt_tooltip_code').val();

			var title = $(this).attr('title');
			$(this).data('ToolTipText', title).removeAttr('title');
			$('<div class="ptdt-tooltip ptdt-hide ptdt_tooltip_bg ptdt_tooltip_color" style="color:' + tooltip + '; background-color:'+ tooltip_bg + ';"></div>').text(title).appendTo('body');
			
			ToolTipTimer  = setTimeout(function(e) {
				$('.ptdt-tooltip').removeClass('ptdt-hide').fadeIn('fast');
		 	},Delay);
	 	}, function() {
			clearTimeout(ToolTipTimer);
			$(this).attr('title', $(this).data('ToolTipText'));
			$('.ptdt-tooltip').remove();
	 	}).mousemove(function(e) {
			var pLeft;
			var pTop;
			var offset = 10;
			var CursorX = e.pageX;
			var CursorY = e.pageY;
			var WindowWidth = $(window).width();
			var WindowHeight = $(window).height();
			var toolTip = $('.ptdt-tooltip');
			var TTWidth = toolTip.width();
			var TTHeight = toolTip.height();			
			if (CursorX-offset >= (WindowWidth/4)*3) {
				pLeft = CursorX - TTWidth - offset;
			} else {
				pLeft = CursorX + offset;
			}
			if (CursorY-offset >= (WindowHeight/4)*3) {
				pTop = CursorY - TTHeight - offset;
			} else {
				pTop = CursorY + offset;
			}
			$('.ptdt-tooltip').css({ top: pTop, left: pLeft })			
	 	});
	}

	function display_setting(){
		// Display setting.
		var primary_color = jQuery('#ptdt_primary_color_code').val();
		var secondary_color = jQuery('#ptdt_secondary_color_code').val();
		var holiday_columns = jQuery('#ptdt_holiday_columns_code').val();
		var general_color = jQuery('#ptdt_general_color_code').val();
		var input_value = jQuery('#ptdt_input_value_code').val();

		// Display style.
		$(".ptdt_primary").css("background-color", primary_color);
		$(".ptdt_secondary_nav").css("color", secondary_color);
		$(".ptdt_secondary").css("border", secondary_color +' solid 1px');
		$(".ptdt_holiday .ptdelivery-number-input").css("background-color", holiday_columns);

		$(".ptdt_general_color").css("color", general_color);
		$(".ptdt_input_color").css("color", input_value);
	}
	function check_variation_changes(){
		
		$('input[name=variation_id]').on('change', function(e) {
			console.log('dfgh');
			if($("input[name=variation_id]").val() != ''){
				var rslt_sum = 0;
				var ptdt_product_price = jQuery('#ptdt_product_price').val();
				var calendar_datas = $('#ptdt_calendar_datas').val();
				var product_id = jQuery('#ptdt_product_id').val();

				// Validate json.
				var calendar_datas_org = [];
				if(calendar_datas != undefined) {
					var calendar_datas_org = JSON.parse(calendar_datas);
				}

		        var rslt_sum = calendar_datas_org['total_quantity'];
				
				// Variation data.
				var form = $("form.cart");
				var variable_product = is_variable_product(form);
				var variation_id = 0;
				if ($("#ptdelivery-calendar").length > 0) {
					if(variable_product == true){
						var variation_id = get_variation_id();
						if(rslt_sum == null || rslt_sum == 0){
							jQuery( "button.single_add_to_cart_button" ).addClass( "variable-pdt-disabled" );
						} else{
							jQuery( "button.single_add_to_cart_button" ).removeClass( "variable-pdt-disabled" );
						}
					}
				}

				var requestData = {};
				var requestData = thwepo_form_price_info();
				var url = thsdf_public_var.ajax_url;
				var price_nonce = thsdf_public_var.update_price_nonce;
				// console.log('xcvb');
				// Validate json.
				var price_info_arr = [];
				if(requestData != undefined){
					var price_info_arr = JSON.stringify(requestData);
				}
				
				jQuery.ajax({
				    url   : url,
				    type  : 'post',
				    data  : {
				       action    			: 'update_product_price',
				       product_id   		: product_id,
				       rslt_sum 			: rslt_sum,
				       ptdt_product_price 	: ptdt_product_price,
				       //priceInfoArr 		: JSON.stringify(requestData),
				       priceInfoArr 		: price_info_arr,
				       is_variable_product 	: variable_product,
				       variation_id 		: variation_id,
				       update_price_nonce : price_nonce,
				    },

				    success : function( response ) {
				    	$('#product_total_price_value').html(response);
				    	$('.quantity .input-text.qty.text').val(rslt_sum);
				    }
				});
			} else{
				$('#product_total_price_value').html('');
			}
		});
	}

	function calculate_quantity(){
		var product_id = jQuery('#ptdt_product_id').val();
		var product_info = {};
		var delivery_info = [];
		var total_quantity = [];
		var uniq = [];
		var array_filtered = {};
		var product_delivery_data = {};
		var product_delivery_qty = {};			
		var i = 0;
		var j = 0;
		var objIndex = null;
		var delivery_date_info = calendar_submited_data();

		if(typeof delivery_date_info == 'undefined' || delivery_date_info == "" || delivery_date_info == null){
			var delivery_info_date = {};
		} else {
			
			var delivery_info_date = delivery_date_info;	
		}

		$("#total_product_quantity").append('<div class="price-ajaxBusy"> <i class="fa fa-spinner" aria-hidden="true"></i></div>');
		$(document).on('change', '.cart .ptdelivery-number-input', function(e) {		
		    $("#product_total_price_value").empty();
		    if($(this).val() < 0){
				$(this).val('');
		    }
		    var delivery_info_per_date ={};
		   	if($(this).val() != null) {
				var product_number = $(this).val();
				var service_date = $(this).data('date');

				//set Minimum price limit for daily purchase.
				var min_price = jQuery('#ptdt_min_price').val();
				var ptdt_product_price = jQuery('#ptdt_product_price').val();
				var allowed_pdt_no = Math.round(min_price/ptdt_product_price);
				var single_total_price = (product_number*ptdt_product_price);
				var pdt_no = product_number;

				
				if(typeof product_number == 'undefined' || product_number == "" || product_number == null){
					delete delivery_info_date[service_date];
				}else{
					var pdt_no = product_number;
					delivery_info_date[service_date]= product_number;
				}

				var time_data = "";
				var delivery_info_date_time = delivery_info_date;		
									
			}
			// Check to disable add-to cart button.
			var form = $("form.cart");
			var variable_product = is_variable_product(form);
			var variation_id = 0;
			if ($("#ptdelivery-calendar").length > 0) {
				var empty_object = jQuery.isEmptyObject(delivery_info_date);
				if(delivery_info_date == "" || delivery_info_date == null || empty_object == true){
					jQuery( ".cart button.single_add_to_cart_button.button" ).addClass( "disabled" );
					if(variable_product == true){		
						jQuery( "button.single_add_to_cart_button" ).addClass( "variable-pdt-disabled" );
					}
				} else {
					jQuery( ".cart button.single_add_to_cart_button.button" ).removeClass( "disabled" );
					if(variable_product == true){
						var variation_id = get_variation_id();
						if(variation_id != null && variation_id != 0 ){	
							jQuery( "button.single_add_to_cart_button" ).removeClass( "variable-pdt-disabled" );
						} else{
							jQuery( "button.single_add_to_cart_button" ).addClass( "variable-pdt-disabled" );
						}
					}
				}
			}
				
			// Total quantity.
			var rslt_sum = array_sum(delivery_info_date);
			
			var ptdt_product_price = jQuery('#ptdt_product_price').val();

			///////THWEPO compatibility (price info)/////////////////
			var requestData = {};
			var requestData = thwepo_form_price_info();

			// Variation data.
			var form = $("form.cart");
			var variable_product = is_variable_product(form);
			var variation_id = 0;
			if(variable_product == true){
				var variation_id = get_variation_id();			
			}

			$('.price-ajaxBusy').show();
			var url = thsdf_public_var.ajax_url;
			var price_nonce = thsdf_public_var.update_price_nonce;
			// console.log(price_nonce);
			// Validate json.
			var price_info_arr = [];
			if(requestData != undefined) {
				var price_info_arr = JSON.stringify(requestData);
			}
			jQuery.ajax({
			    url   : url,
			    type  : 'post',
			    data  : {
			       action    : 'update_product_price',
			       product_id   : product_id,
			       rslt_sum : rslt_sum,
			       ptdt_product_price : ptdt_product_price,
			       // priceInfoArr : JSON.stringify(requestData),
			       priceInfoArr : price_info_arr,
			       is_variable_product 	: variable_product,
			       variation_id : variation_id,
			       update_price_nonce : price_nonce,
			    },

			    success : function( response ) {
			    	$('.price-ajaxBusy').hide();
			    	$('#product_total_price_value').html(response);
			    }
			});


			//////////
			
			var total_price = rslt_sum * ptdt_product_price;
		    var total_price_formated = currencyFormat(total_price);

			

			
			// Product details Array
			delivery_info_per_date['products_per_date'] = delivery_info_date_time;
			delivery_info_per_date['total_quantity'] = rslt_sum;
			
		    var product_delivery_qty = delivery_info_per_date;

		    // Encript array.
		    // Validate json.
		    var product_delivery_details = [];
		    if(product_delivery_qty != undefined) {
		    	var product_delivery_details = JSON.stringify(product_delivery_qty);
		    }

		    // $('#product_total_price_value').html(total_price_formated);
		    $('#ptdt_calendar_datas').val(product_delivery_details);
			$('.quantity .input-text.qty.text').val(rslt_sum);
		});

	}

	jQuery('.single_add_to_cart_button').click(function(){
		if(jQuery(this).is('.disabled.single_add_to_cart_button')){
			event.preventDefault();
		}
	})

	function currencyFormat(num) {

		var ptdt_currency_symbol = jQuery('#ptdt_currency_symbol').val();
	  	return ptdt_currency_symbol + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}

	// Jquery set array sum.
	function array_sum(input_arr){
	   	var total =  0;
	   	$.each( input_arr, function( key, value ){
			total += Number(value);
	   	});
		return total;
	}

	function calendar_submited_data(){
		// Calendar sumbmited data.
		var calendar_datas = jQuery('#ptdt_calendar_datas').val();
		// console.log(calendar_datas);

		// Validate json.
		var obj_calendar_datas = [];
		if(calendar_datas != undefined) {
			var obj_calendar_datas = jQuery.parseJSON(calendar_datas);
		}

		var calendar_entries = {};
		var calendar_saved = {};
		var product_per_date_arr = [];
		if( obj_calendar_datas != null && obj_calendar_datas != ''){
			$.each( obj_calendar_datas, function( key, value ){
				product_per_date_arr = obj_calendar_datas['products_per_date'];
				var total_quantity = obj_calendar_datas['total_quantity'];			
				//jQuery('#product_quantity_value').html(total_quantity);
				var ptdt_product_price = jQuery('#ptdt_product_price').val();
				var ptdt_currency_symbol = jQuery('#ptdt_currency_symbol').val();			
				var total_price = total_quantity * ptdt_product_price;
		    	var total_price_formated = currencyFormat(total_price);
				//var price_field ='<span class="woocommerce-Price-currencySymbol">'+ptdt_currency_symbol+'</span>'+total_price;			
				jQuery('#product_total_price_value').html(total_price_formated);
			
			});

			return product_per_date_arr;
		}
	}
	// cart
	function display_list_of_product_quantity(){
		$('.ptdt-delivery-date-public.full-date-data').hide(); // doubt
		// console.log('bnmnm');
	    $('a.ptdt-read').click(function () {
	        $(this).parent('.ptdt-delivery-date-public.min-date-data').slideUp('fast');
	        console.log('dfgh');
	        $(this).closest('.ptdt-date-qty-content').find('.ptdt-delivery-date-public.full-date-data').slideDown('fast');

	  		// 	$('.mainContent').show();
	        return false;
	    });
	    $('a.ptdt-read-less').click(function () {
	        $(this).parent('.ptdt-delivery-date-public.full-date-data').slideUp('fast');
	        $(this).closest('.ptdt-date-qty-content').find('.ptdt-delivery-date-public.min-date-data').slideDown('fast');
	        return false;
	    });
	}

	function calendar_popup(){
		$(document).ready(function() {
	  		$('.openBtn').click(function(e) {
		     	setTimeout(function() {$('.popup').removeClass('animationClose').addClass('animationOpen');}, 100);
		    	$('.obscure').fadeIn(50);
		    	e.preventDefault();
	  		});

	  		$('.closeBtn').click(function(e) {
		    	e.preventDefault();
		    	setTimeout(function() {$('.obscure').fadeOut(350);}, 50);
		    	$('.popup').removeClass('animationOpen').addClass('animationClose');
	  		});
		});
	}
	function add_special_class(){
		var spec_class = thsdf_public_var.spec_class;
		$(".woocommerce table.shop_table td.product-name").addClass(spec_class);
	}

	// THWEPO compatibility.

	thwepo_calculate_extra_cost();
	function thwepo_calculate_extra_cost(){		
		$(document).on('change', '.thwepo-price-field', function(e) {			
			var rslt_sum = 0;
			var ptdt_product_price = jQuery('#ptdt_product_price').val();
			var calendar_datas = $('#ptdt_calendar_datas').val();
			var product_id = jQuery('#ptdt_product_id').val();

			// Validate json.
			var calendar_datas_org = [];
			if(calendar_datas != undefined) {
				var calendar_datas_org = JSON.parse(calendar_datas);
			}

	        var rslt_sum = calendar_datas_org['total_quantity'];
			
			// Variation data.
			var form = $("form.cart");
			var variable_product = is_variable_product(form);
			var variation_id = 0;
			if(variable_product == true){
				var variation_id = get_variation_id();
			}

			var requestData = {};
			var requestData = thwepo_form_price_info();

			// Validate json.
			var price_info_arr = [];
			if(requestData != undefined) {
				var price_info_arr = JSON.stringify(requestData);
			}
			var url = thsdf_public_var.ajax_url;
			var price_nonce = thsdf_public_var.update_price_nonce;
			jQuery.ajax({
			    url   : url,
			    type  : 'post',
			    data  : {
			       action    			: 'update_product_price',
			       product_id   		: product_id,
			       rslt_sum 			: rslt_sum,
			       ptdt_product_price 	: ptdt_product_price,
			      // priceInfoArr 		: JSON.stringify(requestData),
			       priceInfoArr 		: price_info_arr,
			       is_variable_product	: variable_product,
			       variation_id 		: variation_id,
			       update_price_nonce : price_nonce,
			    },

			    success : function( response ) {
			    	$('#product_total_price_value').html(response);
			    }
			});
		});
	}
	function thwepo_form_price_info(){
		var form = $("form.cart");
		var price_field_elms = $(".thwepo-price-field");
		if (price_field_elms.length > 0) {
			var priceInfoArr = {},
            isVariableProduct = is_variable_product(form),
            productId = get_product_id(),
            variationId = get_variation_id();
			price_field_elms.each(function() {
				var pfield = $(this);
			   	if (is_active_price_field(pfield)) {
                	var ftype = pfield.getType(),
                    multiple = is_multiselect_field(pfield, ftype),
                    id = pfield.prop("id"),
                    name = get_field_name(ftype, pfield.prop("name"), id, multiple),
                    value = get_price_field_value(pfield, ftype, name),
                    quantity = "",
                    label = pfield.data("price-label"),
                    price = pfield.data("price"),
                    priceType = pfield.data("price-type"),
                    priceUnit = pfield.data("price-unit"),
                    priceMinUnit = pfield.data("price-min-unit");

                    if (isInputChoiceField(ftype, multiple)) {
                        var price_props = prepare_price_props_for_selected_options(pfield, name, ftype, multiple);
                        price_props ? (price = price_props.price, priceType = price_props.priceType) : (price = 0, priceType = "")
                    }

                    if (!priceType || "dynamic" !== priceType && "dynamic-excl-base-price" !== priceType ? priceUnit = 0 : !$.isNumeric(priceUnit) && $("#" + priceUnit).length && (quantity = $("#" + priceUnit).val(), priceUnit = 1), value && name && (price || priceType && "custom" === priceType)) {
                        var priceInfo = {};
                        priceInfo.name = name, priceInfo.label = label, priceInfo.value = value, priceInfo.price = price, priceInfo.price_type = priceType, priceInfo.price_unit = priceUnit, priceInfo.price_min_unit = priceMinUnit, priceInfo.quantity = quantity, priceInfo.multiple = multiple, priceInfoArr[name] = priceInfo
                   	}	                    
                }
			});
		}

		var requestData = {};
        requestData.product_id = productId, requestData.price_info = priceInfoArr, requestData.is_variable_product = isVariableProduct, variationId && (requestData.variation_id = variationId);
        return requestData;
	}
	function is_active_price_field(elm) {
        var result = !0;
        return elm.hasClass("thwepo-disabled-field") && (result = !1), result
    }
    function get_field_name(type, name, id, multiple) {
        return "checkbox" == type && multiple ? name = name.replace("[]", "") : "select" == type && multiple && (name = id), name
    }
    function get_price_field_value(elm, type, name) {
        var value = thwepo_public_base.get_field_value(type, elm, name);
        return "radio" === type ? value = elm.is(":checked") ? value : "" : "file" === type && (value = elm.data("file-name"), value = value || ""), value
    }
    function get_field_value(type, elm, name) {
        var value = "";
        switch (type) {
            case "radio":
                value = $("input[type=radio][name=" + name + "]:checked").val();
                break;
            case "checkbox":
                if (1 == elm.data("multiple")) {
                    var valueArr = [];
                    $("input[type=checkbox][name='" + name + "[]']:checked").each(function() {
                        valueArr.push($(this).val())
                    }), value = valueArr
                } else value = $("input[type=checkbox][name=" + name + "]:checked").val();
                break;
            case "select":
            case "multiselect":
            default:
                value = elm.val()
        }
        return value
    }
    function is_variable_product(form) {
        var is_var_product = !1;
        return form.hasClass("variations_form") && (is_var_product = !0), is_var_product
    }
    function isInputChoiceField(type, multiple) {
        return !!("select" === type || "radio" === type || "checkbox" === type && multiple)
    }
    function is_multiselect_field(elm, type) {
        var multiple = 0;
        return "checkbox" == type ? multiple = elm.data("multiple") : "select" == type && elm.attr("multiple") && (multiple = 1), multiple
    }
    function get_product_id() {
        var product_id = $("input[name=add-to-cart]").val();
        return product_id || (product_id = $("button[name=add-to-cart]").val()), product_id
    }
    function get_variation_id() {
        return $("input[name=variation_id]").val()
    }
    function get_selected_options(elm, name, type, multiple) {
        var options = null;
        return "select" === type ? options = elm.find("option:selected") : "radio" === type ? elm.is(":checked") && (options = elm) : "checkbox" === type && multiple && (options = $("input[type=checkbox][name='" + name + "[]']:checked")), options
    }
    function prepare_price_props_for_selected_options(elm, name, type, multiple) {
        var price_props = null,
            oPrice = "",
            oPriceType = "",
            options = get_selected_options(elm, name, type, multiple);
        return options && (multiple ? options.each(function() {
            var oprice = $(this).data("price"),
                opriceType = $(this).data("price-type");
            oprice && (opriceType = opriceType || "normal", oPrice.trim() && (oPrice += ","), oPriceType.trim() && (oPriceType += ","), oPrice += oprice, oPriceType += opriceType)
        }) : (oPrice = options.data("price"), oPriceType = options.data("price-type"), oPriceType = oPriceType || "normal"), thwepo_public_base.isEmpty(oPrice) || thwepo_public_base.isEmpty(oPriceType) || (price_props = {
            price: oPrice,
            priceType: oPriceType
        })), price_props
    }

})( jQuery );
