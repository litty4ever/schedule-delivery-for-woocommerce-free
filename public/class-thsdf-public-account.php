<?php
/**
 * My Account page functionality of the Product Delivery plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    schedule-delivery-for-woocommerce-free
 * @subpackage schedule-delivery-for-woocommerce-free/public
 */
if(!defined('WPINC')) {	
	die; 
}

if(!class_exists('THSDF_Public_account')) :
 
 	/**
     * Public class for account page.
     */ 
	class THSDF_Public_account {
		private $plugin_name;
		private $version;

		/**
         * Constructor.
         */
		public function __construct() {
			$this->add_hook();
            THSDF_Utils::write_log('sdfgh');
		}

		/**
         * Function for add hook.
         */
		public function add_hook() {
		    $theme_name = THSDF_Utils::check_current_theme();
			if($theme_name == 'avada') {
			    add_action('woocommerce_order_items_table',array($this, 'ptdt_order_details_before_order_table'), 20, 1);
			} else {
			    add_action('woocommerce_order_details_before_order_table',array($this, 'ptdt_order_details_before_order_table'), 20,1);
			}
			add_action('woocommerce_order_item_meta_end',array($this, 'ptdt_order_item_visible_data'), 20, 4);  

			add_action('wp_ajax_order_calendar_view', array($this, 'ptdt_order_calendar_view'), 10);
	    	add_action('wp_ajax_nopriv_order_calendar_view', array($this, 'ptdt_order_calendar_view'), 10);

	    	add_action('wp_ajax_cancel_ordered_item', array($this, 'ptdt_cancel_ordered_item'), 10);
	    	add_action('wp_ajax_nopriv_cancel_ordered_item', array($this, 'ptdt_cancel_ordered_item'), 10);
	    	// add_action('wp_ajax_display_time_range', array($this, 'ptdt_display_time_range'), 10);
	    	// add_action('wp_ajax_nopriv_display_time_range', array($this, 'ptdt_display_time_range'), 10);
		}

		/**
         * Function for create my account page calendar.
         *
         * @param string $month Month info from the ordered item data
         * @param string $year The year info from the ordered item data
         * @param integer $item_id The item id
         */
		public function build_thsdf_account_calendar($month,$year,$item_id) {
			THSDF_Utils::write_log('sdfgh');

			$ptdt_delivery_data = wc_get_order_item_meta($item_id, THSDF_Utils::ORDER_KEY_DELIVERY_DATE, 'true');
			$enc_delivery_data = json_encode($ptdt_delivery_data);
			$calendar = "";
			$calendar .= '<input type="hidden" value="'.esc_attr($enc_delivery_data).'" name="order_item_meta_data" class="order_item_meta_data order_item_meta_data_'.esc_attr($item_id).'">';

			$firstDayOfMonth = mktime(0,0,0,$month,1,$year);
			$numberDays = date('t',$firstDayOfMonth);
			$dateComponents = getdate($firstDayOfMonth);
			$monthName = $dateComponents['month'];
			$monthName_sub = substr(ucfirst($monthName),0,3);
			$dayOfWeek = $dateComponents['wday'];

			// Calandar month Controls.
			$theme_name = THSDF_Utils::check_current_theme();
			$theme_class = '';
			if($theme_name == 'twentytwelve') {
				$theme_class = 'twentytwelve-ptdelivery-ac-order-calendar';
			}
			$calendar .= '<div class="ptdelivery-ac-order-calendar '.esc_attr($theme_class).'">';
				$calendar .= '<div class="ptdelivery-ac-controls">';
					$calendar .= '<div class="ptdelivery-ac-controls-buttons">';
						$calendar .= '<a class="button ptdelivery-ac-prev" id="ac-prev" data-item_id="'.esc_attr($item_id).'"><span class="dashicons dashicons-arrow-left ptdt_secondary_nav" ></span></a>';
						$calendar .= '<div id="ptdelivery-ac-date" class="ptdt_general_color">'.esc_html__($monthName_sub).''.esc_html__($year).'</div>';
						$calendar .= '<a class="button ptdelivery-ac-next" id="ac-next" data-item_id="'.esc_attr($item_id).'"><span class="dashicons dashicons-arrow-right ptdt_secondary_nav" ></span></a>';
					$calendar .= '</div>';
				$calendar .= '</div>';
				$calendar .= '<div class="my-order-calendar">';
					$calendar .= '<table class="ptdelivery-ac-calendar" id="ptdelivery-ac-calendar">';
					$calendar .= '</table>';
				$calendar .= '</div>';
			$calendar .= '</div>';

            // $calendar .= "<div class='time-range-display'>";            
            // 	$calendar .= '<span class="helper"></span>';
            // 	$calendar .= "<div class='time-range-display-wrap'>";
            // 	$calendar .= "</div>";
            // $calendar .= "</div>";
			echo $calendar;
		}

		/**
         * Function for set a calendar icon for navigate the order calendar.
         *
         * @param integer $item_id The item id
         * @param array $item The item details
         * @param array $order The order details
         * @param array $plain_text The plain text
         */
		public function ptdt_order_item_visible_data($item_id, $item, $order, $plain_text) {

			if(!is_order_received_page()) {
				$item = new WC_Order_Item_Product($item_id);
				$product_id = $item->get_product_id();
				$plan_delivery_checkbox = get_post_meta($product_id, THSDF_Utils::POST_KEY_PLAN_DELIVERY_CHECKBOX, true);
				$ptdt_delivery_data = wc_get_order_item_meta($item_id, THSDF_Utils::ORDER_KEY_DELIVERY_DATE, 'true');
				if($plan_delivery_checkbox == 'yes') {
					if(!empty($ptdt_delivery_data)) {
				    	echo '<div class="product_calendar_link" >';
				    		echo '<div class="view-my-order-calendar" data-item_id="'.esc_attr($item_id).'"><span class="dashicons dashicons-calendar-alt" ></span></div>';
				    	echo '</div>';
			    	}
		    	}
		    }
	    }

	    /**
         * Function for set order details before order table.
         *
         * @param array $order The order details
         */
	    public function ptdt_order_details_before_order_table($order) {
	    	$theme_name = '';
	    	$theme_class = '';
	    	$theme_name = THSDF_Utils::check_current_theme();
	    	$theme_class = 'thsdf_'.esc_attr($theme_name).'_user_account_calendar';
	    	if(!is_order_received_page()) {
	    		echo '<div class="ptdelivery_user_account_calendar '.esc_attr($theme_class).'">';
			    	$dateComponents = getdate();
				    $month = $dateComponents['mon']; 			     
				    $year = $dateComponents['year'];
				    $item_id = '';
			   	echo '</div>';
			}
		}

		/**
         * Function for set order calendar view(Ajax function).
         */
		public function ptdt_order_calendar_view() {
			$dateComponents = getdate();
			$month = isset($dateComponents['mon']) ? $dateComponents['mon'] : ''; 			     
			$year = isset($dateComponents['year']) ? $dateComponents['year'] : '';
			$item_id = isset($_POST['item_id']) ? sanitize_text_field($_POST['item_id']) : '';
			$item = new WC_Order_Item_Product($item_id);
			$product_name = $item->get_name();
			echo '<div class="thsdf-user-calendar-pdt-name"><h2>'.esc_html__($product_name, 'schedule-delivery-for-woocommerce').'</h2></div>';
			echo $this->build_thsdf_account_calendar($month, $year, $item_id);
			exit;
		}

		/**
         * To hold the ordered item(Ajax function).
         */
		public function ptdt_cancel_ordered_item() {
			$item_id = isset($_POST['item_id']) ? sanitize_text_field($_POST['item_id']) : '';
			$item_date = isset($_POST['item_date']) ? sanitize_text_field($_POST['item_date']) : '';
			// Update db.
			if($item_id != '' && $item_date != '') {
				$product_delivery_info = array();
				$ptdt_delivery_date = wc_get_order_item_meta($item_id, THSDF_Utils::ORDER_KEY_DELIVERY_DATE, 'true');
				if(!empty($ptdt_delivery_date) && is_array($ptdt_delivery_date)) {
					foreach ($ptdt_delivery_date as $key => $value) {						
						// $time_slot = isset($value['time_range']) ? $value['time_range'] : '';
						if($key == $item_date) {
							$product_delivery_info[$key] = array(
									'quantity' 		=> isset($value['quantity']) ? $value['quantity'] : '',
									'status'		=> '6'
								); 
						} else {
							$product_delivery_info[$key] = array(
									'quantity' 		=> isset($value['quantity']) ? $value['quantity'] : '',
									'status'		=> isset($value['status']) ? $value['status'] : ''
								);
						}
					}
				}
				if(!empty($product_delivery_info)) {
					   wc_update_order_item_meta($item_id, THSDF_Utils::ORDER_KEY_DELIVERY_DATE, $product_delivery_info);
				}
			}
			exit;
		}

		/**
         * Function for display the time range(Ajax function).
         */
		function ptdt_display_time_range(){
			$time_range = $_POST['time_range'];
			$time_range_html = '';
            $time_range_html .= '<div>';
                $time_range_html .= '<div class="time-range-dis-close">&times;</div>';
                    $time_range_html .= '<div class="thsdf-time-range-wrap">';
                    	if(!empty($time_range) && $time_range != 'undefined') {
                       		$time_range_html .= '<span>'.$time_range.'</span>'; 
                       		$time_range_html .= '<div class="chart">';
                       		$x = 0;
                       		
							$pattern = "/[-]/";
							$components = preg_split($pattern, $time_range);
                       		
                       		foreach ($components as $key => $value) {
                       			$time_in_24_hour_format[]  = date("H:i", strtotime($value));

                       		}

                       		for($i=0; $i<=24; $i++) {

                       			// Change start Hour format.
								$start_time = $time_in_24_hour_format[0];                      			
                       			$pattern = "/[:]/";
								$start_tim_comp = preg_split($pattern, $start_time);
								$start_tim = ltrim($start_tim_comp[0], '0');

								// Change end Hour format.
                       			$end_time = $time_in_24_hour_format[1];                      			
                       			$pattern = "/[:]/";
								$end_tim_comp = preg_split($pattern, $end_time);
								$end_tim = ltrim($end_tim_comp[0], '0');

								if( ($start_tim <= $i) && ($i <= $end_tim)) {
									$start_class = 'start-dlv-time-prog';
									$mt_start_cls = 'start-dlv-minute-prog';
								} else {
									$start_class = '';
									$mt_start_cls = '';
								}

								$time_range_html .= '<div class="tick '.$start_class.'" style="left:'.$x.'%">'.$i;
                       			$y = 0; 
                       			if($i < 23) {
	                       			for($j=0; $j<=60; $j++) {
										$start_minute = ltrim($start_tim_comp[1], '0');
										$end_minute = ltrim($end_tim_comp[1], '0');

										if( ($end_tim <= $i) && ($i < $end_tim+1)) {
											if( $j <= $end_minute) {
												$mt_start_cls = 'start-dlv-minute-prog';
											} else {
												$mt_start_cls = '';
											}
										}
										$time_range_html .= '<div class="smal-tick '.$mt_start_cls.'" style="left:'.$y.'%"></div>';
		                       			$y = $y+1.66;
		                       		}
		                       	}

                       			$time_range_html .= '</div>';
                       			$x = $x+4.14;
                       		}
                       		
                       	} else {
                       		$time_range_html .= '<p>Time slot is not setted</p>';
                       	}
                    $time_range_html .= '</div>'; 
                $time_range_html .= '</div>';
            $time_range_html .= '</div>';
            echo $time_range_html;
            exit();
		}
	}
endif;